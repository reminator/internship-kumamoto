import numpy as np
from scipy.special import gammaln, psi

class pybssnmf_poisson():
    def __init__(self):
        self.eps = np.spacing(1)
        self.frequency_num = 512 #fs:16000, window:1024

    def spectrogram_data(self, frequency_num):
        self.frequency_num = frequency_num


    def prior_param_set(self, a_tm, b_tm, a_ve, b_ve):
        #set prior parameters for gamma distribution
        #a:shape, b:scale w.r.t gamma dis, and tm:template, ve:Activation

        self.a_tm = a_tm  #Shape
        self.b_tm = b_tm  # Scale
        self.a_ve = a_ve
        self.b_ve = b_ve


    def bayesian_nmf(self, X,  n_iter=50, Bound = 0, I = 3):
        #initalize
        _, ACTIVATION_WINDOW = X.shape
        a_tm = self.a_tm *  np.ones((self.frequency_num, I))
        b_tm = self.b_tm *  np.ones((self.frequency_num, I))
        a_ve = self.a_ve * np.ones((I, ACTIVATION_WINDOW))
        b_ve = self.b_ve * np.ones((I, ACTIVATION_WINDOW))

        L_t = np.random.gamma(a_tm, b_tm/a_tm)
        L_v = np.random.gamma(a_ve, b_ve/a_ve)
        E_t = L_t.copy()
        E_v = L_v.copy()
        Sig_t = L_t.copy()
        Sig_v = L_v.copy()
        cost = -100.


        for i in range(n_iter):
            #Source sufficient stasistics
            LtLv = np.dot(L_t, L_v)
            tmp = X / LtLv

            Sig_t = L_t * np.dot(tmp, L_v.T)
            Sig_v = L_v * np.dot(L_t.T, tmp)

            #Means
            alpha_tm = a_tm + Sig_t
            beta_tm = 1/(a_tm/b_tm + E_v.sum(axis=1).T)
            E_t = alpha_tm * beta_tm

            alpha_ve = a_ve + Sig_v
            beta_ve = 1/((a_ve/b_ve).T + E_t.sum(axis=0)).T
            E_v = alpha_ve * beta_ve


            if i == n_iter - 1:
                if Bound == 1:
                    gammalnX = gammaln(X + 1)
                    B_logT = np.log(L_t)
                    B_logV = np.log(L_v)

                    cost = -np.sum(np.dot(E_t, E_v) + gammalnX) \
                        + np.sum(-X * (((L_t * B_logT).dot(L_v) + L_t.dot(L_v * B_logV))/LtLv - np.log(LtLv))) \
                        + np.sum(-a_tm / b_tm * E_t - gammaln(a_tm) + a_tm*np.log(a_tm/b_tm)) \
                        + np.sum(-a_ve / b_ve * E_v - gammaln(a_ve) + a_ve*np.log(a_ve/b_ve)) \
                        + np.sum(gammaln(alpha_tm) + alpha_tm * (np.log(beta_tm) + 1)) \
                        + np.sum(gammaln(alpha_ve) + alpha_ve * (np.log(beta_ve) + 1))
                    cost /= (float(ACTIVATION_WINDOW) * 10**6)

                return E_t, Sig_t, L_t,  E_v, cost

            # Means of Logs
            L_t = np.exp(psi(alpha_tm)) * beta_tm
            L_v = np.exp(psi(alpha_ve)) * beta_ve

    def bayesian_ssnmf(self, X, SE_T, SSig_T, SL_T,  n_iter=50, Bound = 0, I = 3):
        #initalize
        _, SI = SE_T.shape
        _, ACTIVATION_WINDOW = X.shape

        a_tm = self.a_tm *  np.ones((self.frequency_num, I))
        b_tm = self.b_tm *  np.ones((self.frequency_num, I))
        a_ve = self.a_ve * np.ones((I + SI, ACTIVATION_WINDOW))
        b_ve = self.b_ve * np.ones((I + SI, ACTIVATION_WINDOW))

        L_t = np.concatenate((np.random.gamma(a_tm, b_tm/a_tm), SL_T), axis=1)
        L_v = np.random.gamma(a_ve, b_ve/a_ve)
        E_t = L_t.copy()
        E_t[:, I:] = SE_T
        E_v = L_v.copy()
        Sig_t = L_t.copy()
        Sig_t[:,I:] = SSig_T
        Sig_v = L_v.copy()
        cost = -100.


        for i in range(n_iter):
            #Source sufficient stasistics
            LtLv = np.dot(L_t, L_v)
            tmp = X / LtLv

            Sig_t[:,:I] = L_t[:,:I] * np.dot(tmp, L_v[:I,:].T)
            Sig_v = L_v * np.dot(L_t.T, tmp)

            #Means
            alpha_tm = a_tm + Sig_t[:,:I]
            beta_tm = 1/(a_tm/b_tm + E_v[:I,:].sum(axis=1).T)
            E_t[:,:I] = alpha_tm * beta_tm

            alpha_ve = a_ve + Sig_v
            beta_ve = 1/((a_ve/b_ve).T + E_t.sum(axis=0)).T
            E_v = alpha_ve * beta_ve


            if i == n_iter - 1:
                if Bound == 1:
                    gammalnX = gammaln(X + 1)
                    L_t = L_t[:,:I]
                    L_v = L_v[:I,:]
                    B_logT = np.log(L_t)
                    B_logV = np.log(L_v)
                    E_t = E_t[:,:I]
                    E_v = E_v[:I,:]

                    a_ve = a_ve[:I,:]
                    b_ve = b_ve[:I,:]
                    alpha_ve = alpha_ve[:I,:]
                    beta_ve = beta_ve[:I,:]

                    cost = -np.sum(np.dot(E_t, E_v) + gammalnX) \
                        + np.sum(-X * (((L_t * B_logT).dot(L_v) + L_t.dot(L_v * B_logV))/LtLv - np.log(LtLv))) \
                        #+ np.sum(-a_tm / b_tm * E_t - gammaln(a_tm) + a_tm*np.log(a_tm/b_tm)) \
                        #+ np.sum(-a_ve / b_ve * E_v - gammaln(a_ve) + a_ve*np.log(a_ve/b_ve)) \
                        #+ np.sum(gammaln(alpha_tm) + alpha_tm * (np.log(beta_tm) + 1)) \
                        #+ np.sum(gammaln(alpha_ve) + alpha_ve * (np.log(beta_ve) + 1))
                    cost /= (float(ACTIVATION_WINDOW) * 10**6)

                return E_t[:, :I], E_v[:I], cost

            # Means of Logs
            L_t[:,:I] = np.exp(psi(alpha_tm)) * beta_tm
            L_v = np.exp(psi(alpha_ve)) * beta_ve


if __name__ == '__main__':



    from pysound import pysound
    pys = pysound()
    pys.fileopen('../sound_data/sn.wav')
    tpys = pysound()
    tpys.fileopen('../sound_data/rn.wav')
    #pys.fileopen('../sound_data/getuyoubi2.wav')

    pys.STFT_init()
    tpys.STFT_init()
    fft_frame = int(np.floor((pys.frames_num - pys.WINDOW)/float(pys.ADVANCE)))
    test_frame = int(np.floor((tpys.frames_num - tpys.WINDOW) / float(tpys.ADVANCE)))

    bnmf = pybssnmf_poisson()
    bnmf.ACTIVATION_WINDOW = 50
    bnmf.spectrogram_data(pys.frequency_num)
    bnmf.prior_param_set(1,1,1,1)



    import time
    stime = time.time()
    fft_data_list = []
    act = bnmf.ACTIVATION_WINDOW
    nmf_data = np.zeros((pys.frequency_num, act))

    nmf_wave = np.zeros(pys.WINDOW)
    temp_fft_list = []
    test_fft_list = []
    #NMF

    ST = []
    SV = []

    tok = 0
    for i in range(fft_frame):
        start = i * pys.ADVANCE
        sound_data = pys.sound_data[:,start:start+pys.WINDOW]
        fft_data = pys.STFT(sound_data)
        temp_fft_list.append(fft_data)
        if i > test_frame:
            tok = 1
        if tok == 0:
            test_data = tpys.sound_data[:,start:start+tpys.WINDOW]
            test_fft_list.append(tpys.STFT(test_data))


        if (i+1)%act == 0:
            bs = i - bnmf.ACTIVATION_WINDOW-30
            print bs

            if tok == 0:
                fft_array = np.array(test_fft_list)[bs:, 0, :].T  # (freq, frame)
                print fft_array.shape


                X = np.absolute(fft_array)
                angle = np.angle(fft_array)
                bnmf.prior_param_set(5, 5, 1, 1)
                ST, SSig_T, SL_T, SV,cost = bnmf.bayesian_nmf(X,  Bound=1, I = 20)
                print 's', cost
            fft_array = np.array(temp_fft_list)[:,0,:].T # (freq, frame)
            X = np.absolute(fft_array)
            angle = np.angle(fft_array)
            bnmf.prior_param_set(1, 1, 1, 1)
            T, V, cost = bnmf.bayesian_ssnmf(X, ST, SSig_T, SL_T, Bound=1, I = 20)
            S = np.dot(T,V)
            print 't',cost
            nmf_data = np.concatenate([nmf_data, S], axis=1)
            S = S*np.exp(1*angle)
            nmf_wdata = pys.ISTFT(S.T)
            for n in range(act):
                nmf_wave = np.concatenate([nmf_wave, np.zeros(pys.ADVANCE)])
                ns = (i +1 - act + n) * pys.ADVANCE

                nmf_wave[ns : ns + pys.WINDOW] += nmf_wdata[n]

            fft_data_list += temp_fft_list
            temp_fft_list = []


    nmf_wave = nmf_wave.reshape((1, nmf_wave.shape[0]))

    output_f = './test.wav'
    pys.wave_write(nmf_wave, output_f)

    print 'Time:',time.time() - stime, ' [sec]'
    fft_data_array = np.array(fft_data_list)
    print '(time frame, channel, freq) = ',fft_data_array.shape

    import matplotlib.pyplot as plt
    x_shape = nmf_data.shape[1] - 50
    nmf_data = nmf_data[:,50:]
    x_data = np.arange(x_shape)
    X,Y = np.meshgrid(x_data,pys.frequency)
    nmf_data[nmf_data > 10000] = 10000
    plt.contourf(X,Y,nmf_data)

    plt.hot()
    plt.show()
