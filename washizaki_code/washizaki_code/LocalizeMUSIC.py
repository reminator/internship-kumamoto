#!/usr/bin/env python
#coding:utf-8
import numpy as  np
import numpy.linalg as lig
import xml.etree.ElementTree as xml
import math
from numba import jit, f8, i8, c16


class LocalizeMUSIC(object):
    def __init__(self):
        self.SOUND_SPEED = 340.

        self.WINDOW = 512
        self.ADVANCE = 160
        self.channel_num = 8
        self.sampling_rate = 16000.
        self.sound_distance = 5.
        self.sound_direction = np.arange(0,360, 1, dtype=np.float)
        self.sound_direction_num = self.sound_direction.shape[0]

        #MUSIC parameter
        self.MUSIC_WINDOW = 30
        self.MUSIC_PERIOD = 30
        self.high_freqency = 4000
        self.low_freqency = 500
        self.NUM_SOURCE = 1

        self.count = 0

        self.freq_data = []
        self.doae_freq_data = []
        self.correlation_data = []






    def set_sound_direction(self,dis, init_d, f_d, interval):
        self.sound_distance = dis
        self.sound_direction = np.arange(init_d, f_d, interval)
    def set_frequency(self, low, high):
        self.high_freqency = high
        self.low_freqency = low




    def mic_position(self, path):
        #Get mic position
        self.tree = xml.parse(path)
        self.elem = self.tree.getroot()
        x, y, z = [], [], []
        for e in self.elem.findall('.//position'):
            x.append(e.get('x'))
            y.append(e.get('y'))
            z.append(e.get('z'))
        self.channel_num = len(x)
        self.mic_pos = np.array([x, y, z], dtype='float32')

    def frequency_init(self):
        self.frequency = np.fft.rfftfreq(self.WINDOW, d=1. / self.sampling_rate)
        self.frequency_num = self.frequency.shape[0]
        self.high_freqency_idx = self.frequency_id(self.high_freqency)
        self.low_freqency_idx = self.frequency_id(self.low_freqency)
        self.freqency_bin_num = int(self.high_freqency_idx - self.low_freqency_idx + 1)

    def frequency_id(self, freq):
        idx = np.abs(self.frequency - freq).argmin()
        return idx
    def music_freq_id(self, freq):
        idx = np.abs(self.frequency - freq).argmin() - self.low_freqency_idx
        return idx
    def direction_id(self, dir):
        idx = np.abs(self.sound_direction - dir).argmin()
        return idx

    def Transfer_function(self):
        #localize init
        self.frequency_init()
        self.music_power = np.ones((self.sound_direction_num, self.freqency_bin_num,
                                    self.channel_num - self.NUM_SOURCE, self.channel_num))
        direction_num = len(self.sound_direction)
        tf = np.zeros((direction_num, self.frequency_num, self.channel_num), dtype=np.complex64)
        freq_repeat_array = np.ones((self.frequency_num, self.channel_num)) \
                            * self.frequency.reshape((self.frequency_num, -1)) *  2 * np.pi


        x = self.sound_distance * np.cos(self.sound_direction * np.pi/180.)
        y = self.sound_distance * np.sin(self.sound_direction * np.pi/180.)
        for i in range(direction_num):
            mic2ss_dis = np.sqrt((self.mic_pos[0] - x[i]) ** 2 + (self.mic_pos[1] - y[i]) ** 2)
            sample_dif = (mic2ss_dis - self.sound_distance)/self.SOUND_SPEED

            sample_dif = np.ones((self.frequency_num, self.channel_num)) \
                         * sample_dif.reshape((-1, self.channel_num))
            #tf[i, :,:] = np.exp(freq_repeat_array * sample_dif)
            tf[i, :,:] = np.cos(freq_repeat_array * sample_dif) + 1j*np.sin(-freq_repeat_array*sample_dif)
            tf[i, :,:] /= np.sqrt((tf[i,:,:] * tf[i,:,:].conj()).real)
        self.tf = tf
        self.tf_music = np.conjugate(tf[:, self.low_freqency_idx:self.high_freqency_idx+1, :])

    def tranfer_load(self, file_name):
        tf = np.load(file_name)
        print tf.shape
        tf = tf.transpose((2,0,1))
        self.tf = tf
        self.tf_music = tf[:, self.low_freqency_idx:self.high_freqency_idx+1,:]
        print self.tf_music.shape
    def localize_MUSIC(self, freq_data):
        self.count += 1
        self.freq_data.append(freq_data)
        freq_data = freq_data[:, self.low_freqency_idx:self.high_freqency_idx + 1]

        #correlation compute
        correlation = np.zeros((self.freqency_bin_num, self.channel_num, self.channel_num), dtype=np.complex)

        for f in range(self.freqency_bin_num):
            correlation[f, :, :] = np.outer(freq_data[:, f],freq_data[:, f].conj())
        #correlation = correlation_compute(freq_data, correlation, self.freqency_bin_num)

        self.correlation_data.append(correlation)
        freq_data_len = len(self.freq_data)
        if (freq_data_len >= self.MUSIC_WINDOW) &  (self.count % self.MUSIC_PERIOD == 0):

            cor_mean = np.mean(np.array(self.correlation_data[:self.MUSIC_WINDOW]), axis=0)
            #eigenvecter estimation
            #v[:,i] is the normalized eigenvector corresponding the eigenvalue v[i]
            #v[fr, :, i]
            #w is arranged in decreasing order
            #v = np.zeros((self.freqency_bin_num,self.channel_num - self.NUM_SOURCE, self.channel_num), dtype=np.complex)
            #for f in range(self.freqency_bin_num):
            #    w,tv = lig.eigh(cor_mean[f])
            #    eig_sort = np.argsort(w)[::-1]
            #    tv = tv[:,eig_sort].T
            #    v[f, :, :] = tv[self.NUM_SOURCE:self.channel_num,:]
            w,v = lig.eigh(cor_mean)
            v = v[:,:,::-1].transpose((0,2,1))
            v = v[:, self.NUM_SOURCE:self.channel_num,:]
            self.music_power[:,:] = 1.
            self.music_power = self.music_power * v
            music_power = self.music_power.transpose((2,0,1,3)) * self.tf_music
            music_power = 1./np.absolute(music_power.sum(axis=3)).sum(axis=0) / float(self.channel_num)
            self.doae_freq_data = self.freq_data[:self.MUSIC_PERIOD]
            self.freq_data = self.freq_data[self.MUSIC_PERIOD:]
            self.correlation_data = self.correlation_data[self.MUSIC_PERIOD:]
            return True, music_power.astype(np.float32)
        return None, None

@jit('c8(c16[:,:],c8[:,:,:], i8)')
def correlation_compute(freq_data, correlation ,freq_num):
    #not use
    #correlation = np.zeros((freq_num, mic_num, mic_num), dtype=np.complex)
    for f in range(freq_num):
        correlation[f,:,:] = np.outer(freq_data[:,f], freq_data[:,f].conjugate())

    return correlation




if __name__ == '__main__':
    from pysound import pysound
    pys = pysound()
    pys.fileopen('./sn3.wav')

    music = LocalizeMUSIC()
    music.mic_position('../config/kurage_micpos.xml')
    music.Transfer_function()
    #music.tranfer_load('./tf_np.npy')




    fft_frame = int(np.floor((pys.frames_num - pys.WINDOW)/float(pys.ADVANCE)))
    pys.STFT_init()
    import time
    stime = time.time()
    fft_data_list = []
    music_power = []
    for i in range(fft_frame):
        start = i * pys.ADVANCE
        sound_data = pys.sound_data[:,start:start+pys.WINDOW]
        fft_data = pys.STFT(sound_data)
        ok, mdata = music.localize_MUSIC(fft_data)
        if ok != None:
            music_power.append(mdata)
        fft_data_list.append(fft_data)

    print 'Time:',time.time() - stime, ' [sec]'
    fft_data_array = np.array(fft_data_list)
    print '(time frame, channel, freq) = ',fft_data_array.shape

    music_power = np.array(music_power)
    #li, hi = music.frequency_id(2700), music.frequency_id(2850)
    li, hi = music.frequency_id(1000), music.frequency_id(2000)
    #li, hi = music.frequency_id(950), music.frequency_id(1050)
    #li, hi = music.frequency_id(2450), music.frequency_id(2550)

    print li, hi
    music_power = music_power[:,:,li:hi].sum(axis=2)

    import matplotlib.pyplot as plt
    from matplotlib.colors import BoundaryNorm
    from matplotlib.ticker import MaxNLocator


    levels = MaxNLocator(nbins=30).tick_values(music_power.min() + 4.3, music_power.max() - 1.0)
    cmap = plt.get_cmap('PiYG')
    norm = BoundaryNorm(levels, ncolors=cmap.N, clip=True)
    x, y = music_power.shape
    x = range(x)
    y = range(y)
    x,y = np.meshgrid(x,y)
    plt.contourf(x,y,music_power.T, levels = levels,
                        cmap=cmap, extend='both')
    plt.hot()
    plt.colorbar()
    plt.show()

    tf = music.tf
    sim = []
    for i in range(music.sound_direction_num):
        s = np.abs(np.sum(tf[i] * tf[180].conj(), axis=1))
        sim.append(s)
    tf = np.array(sim)
    x, y = tf.shape
    plt.figure()
    y = music.frequency[np.arange(y)]
    x = range(x)

    x,y = np.meshgrid(x,y)
    plt.contourf(x,y,tf.T)
    plt.show()
