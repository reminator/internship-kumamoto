#!/usr/bin/env python
#coding:utf-8
import rospy
import numpy as np
from scipy.ndimage.filters import gaussian_filter
from numpy.linalg import norm as np_norm
from LocalizeMUSIC import LocalizeMUSIC
from Bayesian_SSNMF import pybssnmf_poisson
from hark_msgs.msg import HarkSrcFFT
from std_msgs.msg import Float32MultiArray
import time
import math

class sep_NMF():
    def __init__(self):
        #localize param
        self.music = LocalizeMUSIC()
        self.music.frequency_init()

        #frequency_mask
        self.freq_mask = freqency_mask()

        #bayesian_nmf
        self.bnmf = pybssnmf_poisson()
        self.bnmf.ACTIVATION_WINDOW = self.music.MUSIC_PERIOD
        self.bnmf.spectrogram_data(self.music.freqency_bin_num)
        self.bnmf.prior_param_set(0.7,0.7,1,1)
        self.tempACTIV = int(self.bnmf.ACTIVATION_WINDOW / 2.)
        self.nmf_bin = 3

        #nmf updata param
        self.near_deg_th = 15. *math.pi / 180.


        #ros
        rospy.init_node('Separete_NMF')
        hark_fft = rospy.Subscriber('/gHarkSrcFFT', HarkSrcFFT, self.callback)
        self.publish_mask = rospy.Publisher('/freq_Mask', Float32MultiArray, queue_size=10)
        self.pub_freq_mask = Float32MultiArray()

        self.id_list = []
        self.noise_ids = []
        self.noise_ids_count = []
        self.sep_list = []
        self.NMF_list = []
        self.count = 1

        self.noise_check = 5


        #IDs
        self.noise_id = [200, 399]
        self.sound_id = [100, 199]
        self.sound2noise = []


        try:
            rospy.spin()
        except KeyboardInterrupt:
            pass
        plot(self.sep_list)
        plot2(self.sep_list)
    def callback(self, data):
        if data.exist_src_num > 0:
            temp_ids = []
            temp_noise_ids = []
            src_count = 0
            temp_check = 0

            for i, src in enumerate(data.src):

                cdata = np.array([np.array(src.fftdata_real) + 1j * np.array(src.fftdata_imag)])
                if src.id in self.id_list:
                    idx = self.id_list.index(src.id)
                    self.sep_list[idx].sep.append(cdata[0,self.music.low_freqency_idx:self.music.high_freqency_idx+1])
                    self.sep_list[idx].x = src.x
                    self.sep_list[idx].y = src.y
                    self.sep_list[idx].count += 1
                    s = time.time()
                    if self.sep_list[idx].count % self.bnmf.ACTIVATION_WINDOW == 0:

                        if src_count == 0:
                            temp_check = self.data_check(data.src)
                            src_count += 1


                        self.noise_ids_count = filter(lambda x:x+1./self.bnmf.ACTIVATION_WINDOW, self.noise_ids_count)


                        cut_data = np.array(self.sep_list[idx].sep[-self.bnmf.ACTIVATION_WINDOW:])


                        #cut_data = cut_data.reshape((-1, self.bnmf.ACTIVATION_WINDOW))
                        X = np.absolute(cut_data)
                        #angle = np.angle(self.sep_list[idx].sep[-self.bnmf.ACTIVATION:])
                        if ((src.id >= self.noise_id[0] and src.id <= self.noise_id[1]) or (len(self.noise_ids) == 0)) and (temp_check==0):

                            nidx = self.noise_ids.index(src.id)
                            self.noise_ids_count[nidx] = 0

                            self.bnmf.prior_param_set(1, 1, 1, 1)
                            ST, SSig_T, SL_T, SV, cost = self.bnmf.bayesian_nmf(X.T, n_iter=30,Bound=1, I=self.nmf_bin)

                            self.sep_list[idx].T.append(ST)
                            self.sep_list[idx].V.append(SV)
                            self.sep_list[idx].Sig_T.append(SSig_T)
                            self.sep_list[idx].L_T.append(SL_T)
                            print 'noise cost', cost, temp_check


                            Snew = (np.dot(ST,SV)).sum(axis=1)

                            #Snew = gaussian_filter(Snew, 1)

                            hatS = self.freq_mask.calc_n_mask(Snew, self.sep_list[idx].hatS[-1])
                            self.sep_list[idx].hatS.append(hatS)
                            temp_noise_ids.append(idx)

                        elif (src.id >= self.sound_id[0] and src.id <= self.sound_id[1]) and (len(self.noise_ids) > 0) \
                                and (len(self.sep_list[idx].T) > 1) and (temp_check==1):
                            check = 0
                            if len(self.noise_ids) > 0:
                                if self.noise_ids_count[0] <= 2:
                                    nidx = self.id_list.index(self.noise_ids[0])
                                    if len(self.sep_list[nidx].Sig_T) > 1:
                                        T = np.array(self.sep_list[nidx].T[-2])
                                        Sig_T = np.array(self.sep_list[nidx].Sig_T[-2])
                                        L_T = np.array(self.sep_list[nidx].L_T[-2])
                                        check = 1
                            if len(self.noise_ids) > 1:
                                for cind, nid in enumerate(self.noise_ids[1:]):
                                    if self.noise_ids_count[cind+1] <= 2:
                                        nidx = self.id_list.index(nid)
                                        if len(self.sep_list[nidx].Sig_T) > 1:
                                            T = np.concatenate((T,self.sep_list[nidx].T[-2]), axis=1)
                                            Sig_T = np.concatenate((Sig_T,self.sep_list[nidx].Sig_T[-2]),axis=1)
                                            L_T = np.concatenate((L_T,self.sep_list[nidx].L_T[-2]),axis=1)
                            self.bnmf.prior_param_set(1, 1, 1, 1)
                            if check == 1:
                                T, V, cost = self.bnmf.bayesian_ssnmf(X.T, T, Sig_T, L_T,n_iter=30, Bound=1, I=self.nmf_bin)
                                self.sep_list[idx].T.append(T)
                                self.sep_list[idx].V.append(V)
                            elif check == 0:
                                T, SSig_T, SL_T, V, cost = self.bnmf.bayesian_nmf(X.T, n_iter=30, Bound=1, I=self.nmf_bin)
                                self.sep_list[idx].T.append(T)
                                self.sep_list[idx].V.append(V)
                                self.sep_list[idx].Sig_T.append(SSig_T)
                                self.sep_list[idx].L_T.append(SL_T)
                            print 'cost:', cost, temp_check
                            Snew = (np.dot(T,V)).sum(axis=1)
                            hatS = self.freq_mask.calc_s_mask(Snew, self.sep_list[idx].hatS[-1])
                            self.sep_list[idx].hatS.append(hatS)

                            temp_ids.append(idx)

                    if (self.sep_list[idx].count % self.tempACTIV == 0) and (len(self.sep_list[idx].T) < 2) \
                                and (src.id >= self.sound_id[0] and src.id <= self.sound_id[1])  and(temp_check==1):
                        check = 0
                        #temp sound の最初の2回は，ACTIVATIONを小さくして行なう，早めにMUSICにデータを送るため
                        temp = self.bnmf.ACTIVATION_WINDOW / 2
                        cut_data = np.array(self.sep_list[idx].sep[-temp:])
                        X = np.absolute(cut_data)

                        if len(self.noise_ids) > 0:
                            if self.noise_ids_count[0] <= 2:
                                nidx = self.id_list.index(self.noise_ids[0])
                                if len(self.sep_list[nidx].Sig_T) > 1:
                                    T = np.array(self.sep_list[nidx].T[-2])
                                    Sig_T = np.array(self.sep_list[nidx].Sig_T[-2])
                                    L_T = np.array(self.sep_list[nidx].L_T[-2])
                                    check = 1
                        if len(self.noise_ids) > 1:
                            for nindex, nid in enumerate(self.noise_ids[1:]):
                                if self.noise_ids_count[nindex+1] <= 2:
                                    nidx = self.id_list.index(nid)
                                    if len(self.sep_list[nidx].Sig_T) > 1:
                                        T = np.concatenate((T, self.sep_list[nidx].T[-2]), axis=1)
                                        Sig_T = np.concatenate((Sig_T, self.sep_list[nidx].Sig_T[-2]), axis=1)
                                        L_T = np.concatenate((L_T, self.sep_list[nidx].L_T[-2]), axis=1)


                        self.bnmf.prior_param_set(1, 1, 1, 1)
                        self.bnmf.ACTIVATION_WINDOW = self.tempACTIV
                        if check == 1:
                            T, V, cost = self.bnmf.bayesian_ssnmf(X.T, T, Sig_T, L_T, n_iter=30, Bound=1, I=self.nmf_bin)
                            self.sep_list[idx].T.append(T)
                            self.sep_list[idx].V.append(V)
                        elif check == 0:
                            T, SSig_T, SL_T, V, cost = self.bnmf.bayesian_nmf(X.T, n_iter=30, Bound=1, I=self.nmf_bin)
                            self.sep_list[idx].T.append(T)
                            self.sep_list[idx].V.append(V)
                            self.sep_list[idx].Sig_T.append(SSig_T)
                            self.sep_list[idx].L_T.append(SL_T)
                        self.bnmf.ACTIVATION_WINDOW = int(self.tempACTIV * 2)


                        Snew = (np.dot(T, V)).sum(axis=1)
                        hatS = self.freq_mask.calc_s_mask(Snew, self.sep_list[idx].hatS[-1])
                        self.sep_list[idx].hatS.append(hatS)

                        temp_ids.append(idx)



                else:

                    if int(src.id - 200) in self.id_list:
                        ind = self.id_list.index(int(src.id-200))
                        self.id_list[ind] = src.id
                        self.sep_list[ind].ind = src.id

                        self.sound2noise.append(int(src.id-200))
                    elif src.id not in self.sound2noise:
                        self.id_list.append(src.id)
                        self.sep_list.append(separated_sound(src.id))
                        self.sep_list[-1].x = src.x
                        self.sep_list[-1].y = src.y
                        self.sep_list[-1].sep.append(cdata[0,self.music.low_freqency_idx:self.music.high_freqency_idx+1])
                        self.sep_list[-1].hatS.append(self.freq_mask.hatS_init(self.music.freqency_bin_num))
                    if src.id >= self.noise_id[0] and src.id <= self.noise_id[1]:
                        self.noise_ids.append(src.id)
                        self.noise_ids_count.append(0)

            if self.count % 5 == 0:
                self.check_noise()

            if len(temp_noise_ids) > 0:
                self.freq_mask_noise(temp_noise_ids)


            if len(temp_ids) > 0:
                self.freq_mask_calc(temp_ids)


    def data_check(self, datasrc):
        sn_index = [] #fixed sound:0, temp sound:1
        n_index = []
        s_index = []
        temp_conc = 0


        for i, src in enumerate(datasrc):
            if (src.id >= self.noise_id[0] and src.id <= self.noise_id[1]):
                n_index.append(i)
            elif src.id >= self.sound_id[0] and src.id <= self.sound_id[1]:
                s_index.append(i)
                temp_conc = 1
        s_index.extend(n_index)

        return temp_conc#, s_index


    def check_noise(self):

        check = [i for i,v in enumerate(self.noise_ids_count) if v > (self.noise_check)]
        if len(check) > 0:
            print 'noise delite', check
            for ch in check:
                self.noise_ids[ch] = ''
                self.noise_ids_count = ''

            self.noise_ids = filter(lambda s: s != '', self.noise_ids)
            self.noise_ids_count = filter(lambda s:s != '', self.noise_ids_count)

    def freq_mask_noise(self, temp_ids):
        for idind in temp_ids:
            nhatS = self.sep_list[idind].hatS[-1]
            print nhatS[:10]
            self.sep_list[idind].freq_mask.append(nhatS)

            #publish
            self.pub_freq_mask.layout.data_offset = self.sep_list[idind].ind
            self.pub_freq_mask.data = nhatS.tolist()
            self.publish_mask.publish(self.pub_freq_mask)
            #print 'Publish noise mask!', self.sep_list[idind].ind

    def freq_mask_noise2(self, temp_ids):
        nhatS = np.zeros((self.music.freqency_bin_num))
        for idind in temp_ids:
            nhatS += self.sep_list[idind].hatS[-1]
        nhatS /= nhatS.max()

        #publish
        for idind in temp_ids:
            self.sep_list[idind].freq_mask.append(nhatS)
            self.pub_freq_mask.layout.data_offset = self.sep_list[idind].ind
            self.pub_freq_mask.data = nhatS.tolist()
            self.publish_mask.publish(self.pub_freq_mask)
            print 'Publish noise mask!'

    def freq_mask_calc(self, temp_ids):
        for idind in temp_ids:
            x = self.sep_list[idind].x
            y = self.sep_list[idind].y
            hatS = self.sep_list[idind].hatS[-1]
            norm_noise = 0
            nhatS = np.ones_like(hatS)
            tick = 0
            for nind in self.noise_ids:

                nind = self.id_list.index(nind)
                theta = np.absolute(np.arccos((x*self.sep_list[nind].x + y*self.sep_list[nind].y)/(self.music.sound_distance+0.001)**2)*0.999999) /math.pi

                theta = 1 #- min([theta, self.freq_mask.beta_th])
                #norm_noise += theta
                temp_nhatS = theta*self.sep_list[nind].hatS[-1]
                nhatS *= temp_nhatS
            nhatS /= nhatS.max()

            #freq_m = self.freq_mask.alpha_f * hatS + self.freq_mask.bairitu*(1 - nhatS) + self.freq_mask.bairitu
            print 'hs', hatS[:5]
            print 'nhatS', nhatS[:5]
            freq_m = self.freq_mask.alpha_f * hatS* self.freq_mask.beta_f*(1 - nhatS + 0.1*10**9)
            freq_m /= freq_m.max()
            self.sep_list[idind].freq_mask.append(freq_m)

            #publish
            self.pub_freq_mask.layout.data_offset = self.sep_list[idind].ind
            self.pub_freq_mask.data = freq_m.tolist()
            self.publish_mask.publish(self.pub_freq_mask)




class separated_sound():
    def __init__(self, ind):
        self.x = None
        self.y = None
        self.count = 1
        self.ind = ind
        self.sep = []
        self.nmf = []
        self.hatS = []
        self.T = []
        self.V = []
        self.Sig_T = []
        self.L_T = []
        self.x = None
        self.y = None
        self.freq_mask = []


class freqency_mask():
    def __init__(self):
        self.bairitu = 3.

        self.alpha_s = 0.9
        self.beta_s = 0.1

        self.alpha_n = 0.1
        self.beta_n = 0.9


        self.kap_sigma = 2 # over 2
        self.sigma_magni = 1
        self.nth = 0.2
        self.sth = 0.8

        self.alpha_f = 1.
        self.beta_f = 1.
        self.beta_thmax = 0.3
        self.beta_th = 0.3

    def calc_s_mask(self, Snew, Sold):
        #Snew -= Snew.min()
        Snew /= Snew.max()
        Snew[Snew>self.sth] = self.sth
        cos_sim = 1.#(Snew*Sold).sum()/(np_norm(Snew)*np_norm(Sold)) * self.sigma_magni
        hatS = ((self.alpha_s/(self.kap_sigma - cos_sim))*Snew + self.beta_s*Sold)
        hatS /= hatS.max()
        return hatS

    def calc_n_mask(self, Nnew, Nold):
        #Nnew -= Nnew.min()
        Nnew /= Nnew.max()
        Nnew[Nnew > self.nth] = self.nth
        cos_sim = 1.#(Nnew * Nold).sum() / (np_norm(Nnew) * np_norm(Nold)) * self.sigma_magni
        hatS = ((self.alpha_n / (self.kap_sigma - cos_sim)) * Nnew + self.beta_n * Nold)
        hatS /= hatS.max()
        return hatS

    def hatS_init(self, freqency_bin):
        hatS =  np.ones((freqency_bin), dtype=np.float32)
        hatS /= hatS.sum()
        return hatS


def plot(sepids):
    import matplotlib.pyplot as plt
    import matplotlib
    from matplotlib.colors import BoundaryNorm
    from matplotlib.ticker import MaxNLocator
    matplotlib.rcParams['pdf.fonttype'] = 42
    matplotlib.rcParams['ps.fonttype'] = 42
    matplotlib.rc('xtick', labelsize=27)
    matplotlib.rc('ytick', labelsize=27)

    for idx in sepids:

        for i in range(len(idx.T)):
            if i == 0:
                data = np.dot(idx.T[i], idx.V[i])
            if i > 0:
                data2 = np.dot(idx.T[i], idx.V[i])
                data = np.concatenate((data,data2), axis=1)


        y,x = data.shape
        y = np.linspace(500,4000,y)
        x = range(x)
        x,y = np.meshgrid(x,y)
        plt.figure()
        print x.shape, data.shape
        data /= data.max()
        levels = MaxNLocator(nbins=30).tick_values(data.min() + 0, data.max()-0.0)
        cmap = plt.get_cmap('PiYG')



        plt.contourf(x,y,data,levels = levels,
                        cmap=cmap, extend='both')
        plt.xlabel('Time index', fontsize=28)
        plt.ylabel('Frequency [kHz]', fontsize=28)
        plt.yticks([1000,2000,3000,4000], [str(1),str(2),str(3),str(4)])
        plt.hot()
        plt.tight_layout()
        plt.savefig('../fig/' +str(idx.ind) +'_nmf.png', dpi=200)
        #plt.show()

def plot2(sepids):
    import matplotlib.pyplot as plt
    import matplotlib
    from matplotlib.colors import BoundaryNorm
    from matplotlib.ticker import MaxNLocator
    matplotlib.rcParams['pdf.fonttype'] = 42
    matplotlib.rcParams['ps.fonttype'] = 42
    matplotlib.rc('xtick', labelsize=27)
    matplotlib.rc('ytick', labelsize=27)

    for idx in sepids:

        for i in range(len(idx.freq_mask)):
            if i == 0:
                data = idx.freq_mask[i].reshape((-1,1))
            if i > 0:
                data = np.concatenate((data,idx.freq_mask[i].reshape((-1,1))), axis=1)


        y,x = data.shape
        y = np.linspace(500, 4000, y)
        x = range(x)
        x,y = np.meshgrid(x,y)
        plt.figure()
        print x.shape, data.shape
        data /= data.max()
        levels = MaxNLocator(nbins=30).tick_values(data.min() + 0, data.max() - 0.0)
        cmap = plt.get_cmap('PiYG')

        plt.contourf(x,y,data,levels = levels,
                        cmap=cmap, extend='both')
        plt.xlabel('Time index', fontsize=28)
        plt.ylabel('Frequency [kHz]', fontsize=28)
        plt.yticks([1000, 2000, 3000, 4000], [str(1), str(2), str(3), str(4)])
        plt.hot()
        plt.tight_layout()
        plt.savefig('../fig/' +str(idx.ind) +'_mask.png', dpi=200)
        #plt.show()
        plt.figure()
        data = 1/(1+np.exp(-10.*(data-0.5)))
        plt.contourf(x,y,data,levels = levels,
                        cmap=cmap, extend='both')
        plt.xlabel('Time index', fontsize=28)
        plt.ylabel('Frequency [kHz]', fontsize=28)
        plt.yticks([1000, 2000, 3000, 4000], [str(1), str(2), str(3), str(4)])
        plt.hot()
        plt.tight_layout()
        plt.savefig('../fig/' +str(idx.ind) +'_mask_sigmoid.png', dpi=200)



if __name__ == '__main__':
    snmf = sep_NMF()




