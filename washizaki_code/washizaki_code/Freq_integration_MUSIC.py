#!/usr/bin/env python
#coding:utf-8
import rospy
import numpy as np
import math
from scipy import signal
from LocalizeMUSIC import LocalizeMUSIC
from freq_weight import freq_weight_c
from rospy.numpy_msg import numpy_msg
from rospy_tutorials.msg import Floats
from std_msgs.msg import Float32MultiArray

import time
class Freq_MUSIC():
    def __init__(self):
        self.mask_proc = 1
        if self.mask_proc == 1:
            print 'Freqency weighted MUSIC process start'

        self.music = LocalizeMUSIC()
        self.music.frequency_init()
        #Estimation sound direction
        self.music_ith = 0.04
        self.music_thf = 0.001
        self.filter_thf = 0.005
        self.filter_thf2 = 0.0008
        self.music_th = self.music_thf * self.music.freqency_bin_num
        self.music_dir_pi = self.music.sound_direction *np.pi / 180
        self.eps = 0.99999999
        print 'music th:', self.music_th

        self.music_filter = freq_weight_c()

        self.VonMises_direst = VonMises_Direction_Estimation()

        #id
        self.id_control = id_control()

        self.musicp_l = []
        self.music_ml = []
        self.music_power_list =[]
        self.music_npower_list = []
        self.musicp_nl = []
        self.music_nml = []
        self.music_tml = []
        self.music_mresult = []
        self.music_tpower_list = []


        #IDs
        self.noise_idr = [200, 399]
        self.sound_idr = [100, 199]
        #mask
        self.freq_nmaskdic = {}
        self.freq_tmaskdic = {}
        self.nhatmask = np.zeros((self.music.freqency_bin_num))
        self.sumnhatmask = np.zeros((self.music.freqency_bin_num))
        #self.thatmask = np.zeros((self.music.freqency_bin_num))
        #self.thatmask_list = []
        self.noise_mask = None


        # ROS
        rospy.init_node('Freq_MUSIC')
        hark_fft = rospy.Subscriber('/MUSIC_Power', numpy_msg(Floats), self.callback)
        mask_sub = rospy.Subscriber('/freq_Mask', Float32MultiArray, self.callback_mask)
        self.sound_dir_pub = rospy.Publisher('/Temp_sound_dir', numpy_msg(Floats), queue_size=10)
        # FFT init
        self.mpower_diradd = 10
        self.mpower = np.zeros(self.music.sound_direction_num+self.mpower_diradd*2, dtype=np.float32)
        self.count = 0

        try:
            rospy.spin()
        except KeyboardInterrupt:
            pass
        #np.save('music_power.npy', np.array(self.music_power_list))
        #self.music_power_list = np.array(self.music_power_list, dtype=np.float32)
        #np.save('music_npower.npy', self.music_power_list)
        mplot(self.music.sound_direction, self.musicp_l, self.music_ml, 'normal')
        mplot(self.music.sound_direction,self.musicp_nl, self.music_nml, 'nmask')
        mplot(self.music.sound_direction,self.musicp_nl, self.music_tml, 'tmask')
        mplot(self.music.sound_direction,self.musicp_nl, self.music_mresult, 'reuslt')
        musicpower_plot(np.array(self.music_power_list, dtype=np.float32), 'm')
        musicpower_plot(np.array(self.music_npower_list, dtype=np.float32), 'n')
        musicpower_plot(np.array(self.music_tpower_list, dtype=np.float32), 't')

    def callback_mask(self, data):
        data_id = int(data.layout.data_offset)

        if (self.noise_idr[0] <= data_id and data_id <= self.noise_idr[1]) \
                and (data_id in self.id_control.nid_array):
            self.freq_nmaskdic[data_id] = np.array(data.data)
        elif (self.sound_idr[0] <= data_id and data_id <= self.sound_idr[1]) \
                and (data_id in self.id_control.tid_array):
            self.freq_tmaskdic[data_id] = np.array(data.data)
        else:
            print '#Unkown ID input!!!',data_id



    def freq_weight_music(self, mpower):

        uniform_dir = []
        uniform_Ids = []
        temp_dir = []
        temp_Ids = []
        if len(self.id_control.nid_array) > 0:
            dic_ok = 0
            self.nhatmask[:]  = 1.
            self.sumnhatmask[:] = 0.
            for nid in self.id_control.nid_array:
                if nid in self.freq_nmaskdic:
                    temp_mask = self.freq_nmaskdic[nid]

                    self.sumnhatmask[temp_mask > self.sumnhatmask] = temp_mask[temp_mask > self.sumnhatmask]
                    self.nhatmask *= temp_mask

                    temp_mask = self.music_filter.sigmoid(temp_mask)
                    nmpower = self.music_filter.music_filter(mpower, temp_mask)
                    nmpower = nmpower.sum(axis=1)
                    mth = (self.filter_thf * temp_mask * self.music_filter.thfilter).sum()
                    maxdir, maxId = self.return_maxdir(nmpower, mth)
                    uniform_dir.extend(maxdir.tolist())
                    uniform_Ids.extend(maxId.tolist())



                    dic_ok = 1

            if dic_ok == 0:
                self.nhatmask[:] = 0.

            elif dic_ok == 1:

                self.nhatmask /= self.nhatmask.max()
                tmask = self.music_filter.sigmoid(self.nhatmask)
                tmask =  1 - tmask

                nmpower = self.music_filter.music_filter(mpower, tmask)
                self.music_npower_list.append(nmpower)
                nmpower = nmpower.sum(axis=1)
                self.musicp_nl.append(nmpower)
                mth = (self.filter_thf * self.nhatmask*self.music_filter.thfilter).sum()
                maxdir, maxId = self.return_maxdir(nmpower, mth)
                uniform_dir.extend(maxdir.tolist())
                uniform_Ids.extend(maxId.tolist())

                self.sumnhatmask /= self.sumnhatmask.max()
                tmask = self.music_filter.sigmoid(self.sumnhatmask)
                tmask =  1 - tmask


                nmpower = self.music_filter.music_filter(mpower, tmask)
                nmpower = nmpower.sum(axis=1)


                mth = (self.filter_thf * self.sumnhatmask*self.music_filter.thfilter).sum()
                print 'th',mth, nmpower.max()
                maxdir, maxId = self.return_maxdir(nmpower, mth)
                uniform_dir.extend(maxdir.tolist())
                uniform_Ids.extend(maxId.tolist())

        else:
            self.music_npower_list.append(np.zeros_like(mpower))
            other = np.ones((self.music.sound_direction_num))
            other /= other.sum()
            self.musicp_nl.append(other)
        if len(self.id_control.tid_array) > 0:
            '''
            if len(self.id_control.nid_array) > 0:
                for nid in self.id_control.nid_array:
                    if nid in self.freq_nmaskdic:
                        temp_mask = self.freq_nmaskdic[nid]
                        nmpower = (mpower *temp_mask)
                        nmpower = nmpower.sum(axis=1)
                        mth = (self.music_thf * temp_mask).sum()
                        maxdir, maxId = self.return_maxdir(nmpower, mth)
                        uniform_dir.extend(maxdir.tolist())
                        uniform_Ids.extend(maxId.tolist())
            '''
            dic_ok = 0
            #self.thatmask_list = []
            for tid in self.id_control.tid_array:
                if tid in self.freq_tmaskdic:
                    temp_mask = self.freq_tmaskdic[tid]
                    temp_mask = self.music_filter.sigmoid(temp_mask)
                    tmpower = self.music_filter.music_filter(mpower, temp_mask)
                    self.music_tpower_list.append(tmpower)

                    tmpower = tmpower.sum(axis=1)

                    mth = (self.filter_thf2 * self.freq_tmaskdic[tid]**self.music_filter.thfilter).sum()
                    print 'thm', mth, tmpower.max()
                    maxdir, maxId = self.return_maxdir(tmpower, mth)
                    temp_dir.extend(maxdir.tolist())
                    temp_Ids.extend(maxId.tolist())
                    dic_ok = 1


        if len(uniform_dir) > 0:
            uniform_dir = sorted(set(uniform_dir), key = uniform_dir.index)
            uniform_Ids = sorted(set(uniform_Ids), key = uniform_Ids.index)
            temp_dir = sorted(set(temp_dir), key = temp_dir.index)
            temp_Ids = sorted(set(temp_Ids), key = temp_Ids.index)
        return np.array(uniform_dir), np.array(uniform_Ids, dtype=np.int32), \
               np.array(temp_dir), np.array(temp_Ids, dtype=np.int32)





    def callback(self, data):

        s = time.time()
        mpower = data.data.reshape((self.music.sound_direction_num, self.music.freqency_bin_num)).copy()
        self.music_power_list.append(mpower)

        uniform_dir, uniform_id, mpower = self.uniform_freq_music(mpower)
        if self.mask_proc == 1:
            nmask_dir, nmask_ids,tmask_dir, tmask_ids = self.freq_weight_music(mpower)
            uniform_dir = np.concatenate((uniform_dir, nmask_dir, tmask_dir))
            #uniform_id2 =  np.concatenate((uniform_id, nmask_ids, tmask_ids))
            uniform_dir = np.unique(uniform_dir)
            #uniform_id2 = np.unique(uniform_id)

        #For plot
            result_dir = np.concatenate((nmask_dir, tmask_dir))
            result_dir = np.unique(result_dir)
        self.music_nml.append(nmask_dir)
        self.music_tml.append(tmask_dir)
        self.music_mresult.append(result_dir)

        self.VonMises_direst.RB_updata(uniform_id)
        Vondir, Vonid = self.return_maxdir(self.VonMises_direst.poster_dis, self.VonMises_direst.prob_th)
        Vondir = self.VonMises_direst.RB_maxID(Vondir)

        self.id_control.noise_dirs_input(Vondir)
        self.id_control.temp_dirs_input(uniform_dir)
        ids, dirs = self.id_control.return_True_direction()
        pub_data = np.append(ids, dirs).astype(np.float32)
        self.sound_dir_pub.publish(pub_data)

        #print
        if len(self.id_control.tdir_array) > 0:
            print 'music observation:', uniform_dir
            print 'noise:', self.id_control.nid_array, self.id_control.ndir_array
            print 'temp:', self.id_control.tid_array, self.id_control.tdir_array
            print 'return:', dirs
            print time.time() - s,'secs'


    def uniform_freq_music(self, mpower):

        mth = np.where(mpower < self.music_ith)
        if mth[0].shape[0] > 0:
            mpower[mth[0], mth[1]] = self.music_ith/100.
        freq_inte_mpower = mpower.sum(axis=1)
        maxdir, maxId = self.return_maxdir(freq_inte_mpower, self.music_th)
        self.musicp_l.append(freq_inte_mpower)
        self.music_ml.append(maxdir)
        return maxdir, maxId, mpower

    def return_maxdir(self, dir_array, th):
        self.mpower[:self.mpower_diradd] = dir_array[-self.mpower_diradd:]
        self.mpower[-self.mpower_diradd:] = dir_array[:self.mpower_diradd]
        self.mpower[self.mpower_diradd:-self.mpower_diradd] = dir_array

        maxId = signal.argrelmax(self.mpower, order=3)[0]
        maxId = maxId[np.where((self.mpower_diradd-1 < maxId) &
                      (maxId < (self.music.sound_direction_num + self.mpower_diradd))
                        & (self.mpower[maxId] > th))[0]]
        maxId -= self.mpower_diradd
        max_dir = self.music.sound_direction[maxId]

        return max_dir,maxId

class VonMises_Direction_Estimation(LocalizeMUSIC):
    '''
    定常雑音を観測する確率分布
    '''
    def __init__(self):
        super(VonMises_Direction_Estimation, self).__init__()
        self.eps = 0.99999999
        kap = 70
        theta_th = 5

        self.same_th = 5 * np.pi / 180.
        self.oktimes = 5

        self.prob_th = 0.010#(self.sound_direction_num)
        self.noise_dirs = np.array([], dtype=np.float32)
        self.noise_dirs_count = np.array([], dtype=np.int32)
        self.x = np.array([], dtype=np.float32)
        self.y = np.array([], dtype=np.float32)

        dir_rad = self.sound_direction * np.pi / 180
        unit_vec = np.array([np.cos(dir_rad), np.sin(dir_rad)])
        self.likelihood = np.zeros((self.sound_direction_num, self.sound_direction_num), dtype=np.float32)

        for i, drad in enumerate(dir_rad):
            self.likelihood[i] = np.exp(kap * np.dot(unit_vec[:,i] , unit_vec[:,]))



        temp_th = self.likelihood[0, theta_th]
        self.likelihood[self.likelihood < temp_th] = temp_th
        self.likelihood /= self.likelihood.sum(axis=1)


        self.ones_dis = np.ones_like(self.likelihood[0])
        self.ones_dis /= self.ones_dis.sum()
        self.poster_dis = self.ones_dis.copy()
        #for recursive bayesian predict

        pred = np.arange(1,9)/9.
        invpred = pred[:-1]
        self.predict_kernel = np.concatenate((pred, invpred[::-1]))
        self.predict_kernel /= self.predict_kernel.sum()
        kernel_num = self.predict_kernel.shape[0]
        self.pos_dadd = (kernel_num - 1) / 2
        self.poster_dis_add = np.zeros(self.sound_direction_num + self.pos_dadd * 2, dtype=np.float32)
        self.posdis_list = []
        self.count = 0

    def RB_updata(self, thetas):
        print 'RBT', thetas
        self.noise_dirs_count += 1
        obdir = []
        for direction in thetas:
            x = np.array([math.cos(direction * math.pi / 180.)])
            y = np.array([math.sin(direction * math.pi / 180.)])
            theta = np.absolute(np.arccos(((x*self.x + y*self.y)*self.eps)))
            wtheta = np.where(theta < self.same_th)[0]
            if wtheta.shape[0] > 0:
                self.noise_dirs_count[wtheta] = 0
            else:
                obdir.append(direction)

        thetas = np.array(obdir)
        #predict non shift
        print 'RBT',thetas, self.noise_dirs
        self.poster_dis_add[:self.pos_dadd] = self.poster_dis[-self.pos_dadd:]
        self.poster_dis_add[-self.pos_dadd:] = self.poster_dis[:self.pos_dadd]
        self.poster_dis_add[self.pos_dadd:-self.pos_dadd] = self.poster_dis
        for i in range(1):
            self.poster_dis = np.convolve(self.poster_dis_add, self.predict_kernel, 'valid')
        #updata
        num_thetas = thetas.shape[0]
        likelihood = self.ones_dis.copy()
        if num_thetas > 0:
            for i, dir in enumerate(obdir):
                likelihood[self.likelihood[int(dir)] > likelihood] = \
                    self.likelihood[int(dir)][self.likelihood[int(dir)] > likelihood]

            self.poster_dis *= likelihood
        #th = self.prob_th / (self.sound_direction_num*2)
        #self.poster_dis[self.poster_dis > th] = th
        self.poster_dis /= self.poster_dis.sum()
        self.posdis_list.append(self.poster_dis)
        print 'dis max', self.poster_dis.max()


        noise_del_count = np.where(self.noise_dirs_count > self.oktimes)[0]
        if noise_del_count.shape[0] > 0:
            for idx in noise_del_count:
                nid_ok_array = np.delete(self.noise_dirs, idx)
                nid_ok_count = np.delete(self.noise_dirs_count, idx)
                nx = np.delete(self.x, idx)
                ny = np.delete(self.y, idx)

            self.noise_dirs = nid_ok_array
            self.noise_dirs_count = nid_ok_count
            self.x = nx
            self.y = ny


    def RB_maxID(self, Vondir):
        tick = 0
        for dir in Vondir:
            x = np.array([math.cos(dir * math.pi / 180.)])
            y = np.array([math.sin(dir * math.pi / 180.)])
            theta = np.absolute(np.arccos((x * self.x + y * self.y)*self.eps))
            wtheta = np.where(theta < self.same_th)[0]
            if wtheta.shape[0] == 0:
                tick = 1
                self.noise_dirs = np.append(self.noise_dirs, np.array([dir]))
                self.noise_dirs_count = np.append(self.noise_dirs_count, np.array([0]))
                self.x =  np.append(self.x, np.array(math.cos(dir * math.pi / 180.)))
                self.y =  np.append(self.y, np.array(math.sin(dir * math.pi / 180.)))
        if tick == 1:
            self.poster_dis_add[:self.pos_dadd] = self.poster_dis[-self.pos_dadd:]
            self.poster_dis_add[-self.pos_dadd:] = self.poster_dis[:self.pos_dadd]
            self.poster_dis_add[self.pos_dadd:-self.pos_dadd] = self.poster_dis
            print 'Create noise!'
            for i in range(5):
                self.poster_dis = np.convolve(self.poster_dis_add, self.predict_kernel, 'valid')
            self.poster_dis /= self.poster_dis.sum()
        return self.noise_dirs

class id_control():
    def __init__(self):
        self.eps = 0.999999
        self.nid_array = []
        self.ndir_array = []
        self.nx = np.array([], dtype=np.float32)
        self.ny = np.array([], dtype=np.float32)
        self.nid_ok_array = np.array([], dtype=np.bool)
        self.nid_ok_count = np.array([], dtype=np.int32)
        self.tid_array = []
        self.tdir_array = []
        self.tx = []
        self.ty = []
        self.tid_ok_count = []
        self.tid_ok_array = []

        self.temp2noise_idlist = []

        self.t_times = 5
        self.t_oktimes = 3
        self.t_same_th = 6. * math.pi/180
        self.cnoise_id_init = 200

        self.temp_id_init = 100

        self.same_th = 6. * math.pi /180.
        self.oktimes = 5

    def noise_dirs_input(self, dirs):
        self.nid_ok_array[:] = True
        self.nid_ok_count[:] += 1
        tx = np.array(self.tx)
        ty = np.array(self.ty)
        for i, direction in enumerate(dirs):
            x = np.array([math.cos(direction * math.pi / 180.)])
            y = np.array([math.sin(direction * math.pi / 180.)])
            theta = np.absolute(np.arccos((x*tx + y*ty)*self.eps))
            wtheta = np.where(theta < self.same_th)[0]
            if wtheta.shape[0] > 0:
                #create noise direction from temp direction
                tid = wtheta[0]
                self.nid_array.append(int(self.tid_array[tid]+200))#self.tid_array[tid])
                self.ndir_array.append(self.tdir_array[tid])
                self.nx = np.append(self.nx, self.tx[tid])
                self.ny = np.append(self.ny, self.ty[tid])
                self.nid_ok_array = np.append(self.nid_ok_array, np.array([True], dtype=np.bool))
                self.nid_ok_count = np.append(self.nid_ok_count, np.array([1], dtype=np.int32))
                self.temp2noise_idlist.append(tid)
                #self.cnoise_id_init += 1

            elif not len(self.nid_array):
                self.nid_array.append(self.cnoise_id_init)
                self.ndir_array.append(direction)

                self.nx = np.append(self.nx, x)
                self.ny = np.append(self.ny, y)
                self.nid_ok_array = np.append(self.nid_ok_array, np.array([True], dtype=np.bool))
                self.nid_ok_count = np.append(self.nid_ok_count, np.array([1], dtype=np.int32))
                self.cnoise_id_init += 1
            else:
                theta = np.absolute(np.arccos((x*self.nx + y*self.ny)*self.eps))
                theta_num = np.where(theta < self.same_th)[0]

                if theta_num.shape[0]:
                    self.nid_ok_array[theta_num] = True
                    self.nid_ok_count[theta_num] = 1
                elif not theta_num.shape[0]:
                    #new noise direction
                    self.nid_array.append(self.cnoise_id_init)

                    self.ndir_array.append(direction)
                    self.nx = np.append(self.nx, x)
                    self.ny = np.append(self.ny, y)
                    self.nid_ok_array = np.append(self.nid_ok_array, np.array([True], dtype=np.bool))
                    self.nid_ok_count = np.append(self.nid_ok_count, np.array([1], dtype=np.int32))
                    self.cnoise_id_init += 1

        noise_del_count = np.where(self.nid_ok_count > self.oktimes)[0]
        if noise_del_count.shape[0] > 0:
            delete_idx = []
            for idx in noise_del_count:
                #print idx, noise_del_count, self.nid_ok_count, self.ndir_array, self.nid_ok_array
                self.ndir_array[idx] = ''
                self.nid_array[idx] = ''
                delete_idx.append(idx)

            self.nid_array = filter(lambda s: s != '', self.nid_array)
            self.ndir_array = filter(lambda s: s != '', self.ndir_array)

            self.nid_ok_array = np.delete(self.nid_ok_array, delete_idx)
            self.nid_ok_count = np.delete(self.nid_ok_count, delete_idx)
            self.nx = np.delete(self.nx, delete_idx)
            self.ny = np.delete(self.ny, delete_idx)



        if len(self.temp2noise_idlist) > 0:
            for idk in self.temp2noise_idlist:
                self.tid_array[idk] = ''
                self.tdir_array[idk] = ''
                self.tx[idk] = ''
                self.ty[idk] = ''
                self.tid_ok_count[idk] = ''
                self.tid_ok_array[idk] = ''
            self.tid_array = filter(lambda s: s != '', self.tid_array)
            self.tdir_array = filter(lambda s: s != '', self.tdir_array)
            self.tx = filter(lambda s: s != '', self.tx)
            self.ty = filter(lambda s: s != '', self.ty)
            self.tid_ok_count = filter(lambda s: s != '', self.tid_ok_count)
            self.tid_ok_array = filter(lambda s: s != '', self.tid_ok_array)
            self.temp2noise_idlist = []

    def temp_dirs_input(self, dirs):
        self.tid_ok_count = map(lambda x:x+1,self.tid_ok_count)
        self.tid_ok_array = map(lambda x:True, self.tid_ok_array)
        for i, direction in enumerate(dirs):
            x = math.cos(direction * math.pi / 180.)
            y = math.sin(direction * math.pi / 180.)
            theta = np.absolute(np.arccos((x*self.nx + y*self.ny)*self.eps))
            if not self.nid_ok_array[theta < self.same_th].shape[0]:
                if not len(self.tid_array):
                    self.tid_array.append(self.temp_id_init)
                    self.tdir_array.append(direction)
                    self.tx.append(x)
                    self.ty.append(y)
                    self.tid_ok_count.append(1)
                    self.tid_ok_array.append(True)
                    self.temp_id_init += 1
                else:
                    ###############################################################

                    theta = np.absolute(np.arccos((x * np.array(self.tx) + y * np.array(self.ty))*self.eps))

                    theta_thnum = np.where(theta < self.t_same_th)[0]
                    if theta_thnum.shape[0]:
                        for idk in theta_thnum:
                            self.tdir_array[idk] = direction
                            self.tx[idk] = x
                            self.ty[idk] = y
                            self.tid_ok_count[idk] = 1
                            self.tid_ok_array[idk] = True

                    elif not theta_thnum.shape[0]:
                        self.tid_array.append(self.temp_id_init)
                        self.tdir_array.append(direction)
                        self.tx.append(x)
                        self.ty.append(y)
                        self.tid_ok_count.append(1)
                        self.tid_ok_array.append(True)
                        self.temp_id_init += 1
        self.ok_add()
        self.del_count_max()

    def return_True_direction(self):
        ndir = np.array(self.ndir_array)[self.nid_ok_array]
        nid = np.array(self.nid_array)[self.nid_ok_array]
        #print self.nid_ok_array

        tok = np.array(self.tid_ok_array, dtype=np.bool)
        tdir = np.array(self.tdir_array)[tok]
        tid = np.array(self.tid_array)[tok]

        ids = np.append(nid, tid)
        dirs = np.append(ndir, tdir)
        return ids, dirs
    def ok_add(self):
        for i, count in enumerate(self.tid_ok_count):
            if count <= self.t_oktimes:
                self.tid_ok_array[i] = True

    def del_count_max(self):
        u, index = np.unique(self.tdir_array, return_index=True)
        tf_index = np.zeros_like(self.tdir_array, dtype=np.bool)
        tf_index[index] = True
        tid_ok_count = np.array(self.tid_ok_count)
        #print 'index', tf_index
        tid_ok_count[~tf_index] = self.t_times + 2
        max_ids = np.where(tid_ok_count > self.t_times)[0]
        for idk in max_ids:
            self.tid_array[idk] = ''
            self.tdir_array[idk] = ''
            self.tx[idk] = ''
            self.ty[idk] = ''
            self.tid_ok_count[idk] = ''
            self.tid_ok_array[idk] = ''
        self.tid_array = filter(lambda  s:s != '', self.tid_array)
        self.tdir_array = filter(lambda  s:s != '', self.tdir_array)
        self.tx = filter(lambda  s:s != '', self.tx)
        self.ty = filter(lambda  s:s != '', self.ty)
        self.tid_ok_count = filter(lambda  s:s != '', self.tid_ok_count)
        self.tid_ok_array = filter(lambda  s:s != '', self.tid_ok_array)











def mplot(dir, mp, maxmp, name):
    import matplotlib.pyplot as plt
    import matplotlib
    from matplotlib.colors import BoundaryNorm
    from matplotlib.ticker import MaxNLocator
    matplotlib.rcParams['pdf.fonttype'] = 42
    matplotlib.rcParams['ps.fonttype'] = 42
    matplotlib.rc('xtick', labelsize=27)
    matplotlib.rc('ytick', labelsize=27)
    #music power plot
    data = np.array(mp)




    #data /= data.max()
    tn, dn = data.shape
    tn = range(tn)
    x, y = np.meshgrid(tn, dir)
    plt.figure()
    dm = data[:,:50].max()
    levels = MaxNLocator(nbins=30).tick_values(data.min() + 0, dm+0.5)
    cmap = plt.get_cmap('PiYG')

    plt.contourf(x, y, data.T, levels=levels,
                 cmap=cmap, extend='both')
    plt.xlabel('Time index', fontsize=28)
    plt.ylabel('Azimuth [deg]', fontsize=28)
    plt.ylim([0, 40])
    plt.hot()
    plt.colorbar()
    plt.tight_layout()
    plt.savefig('../fig/music_power_'+name+'.png', dpi=200)

    plt.figure()
    dm = data.max()
    levels = MaxNLocator(nbins=30).tick_values(data.min() + 0, dm+0.5)
    cmap = plt.get_cmap('PiYG')

    plt.contourf(x, y, data.T, levels=levels,
                 cmap=cmap, extend='both')
    plt.xlabel('Time index', fontsize=28)
    plt.ylabel('Azimuth [deg]', fontsize=28)
    plt.ylim([0, 360])
    plt.hot()
    plt.colorbar()
    plt.tight_layout()
    plt.savefig('../fig/music_power_'+name+'_a.png', dpi=200)

    #maxdir plot
    plt.figure()
    tn = len(maxmp)
    tn = range(tn)
    plt.scatter([0], [-50], c='r', s=40, linewidths="2", edgecolors='r')
    for t in tn:
        mmp = maxmp[t]
        mmpl = len(mmp)
        mmpl = np.ones((mmpl)) * t
        plt.scatter(mmpl, mmp,c='r', s=40, linewidths="2", edgecolors='r')
    plt.ylabel('Azimuth [deg]', fontsize=28)
    plt.xlabel('Time index', fontsize=28)
    plt.ylim([0, 40])

    plt.tight_layout()
    plt.savefig('../fig/max_dirs_'+name+'.png', dpi=200)

    plt.figure()
    tn = len(maxmp)
    tn = range(tn)
    #plt.scatter([0], [-50], c='r', s=40, linewidths="2", edgecolors='r')
    for t in tn:
        mmp = maxmp[t]
        mmpl = len(mmp)
        mmpl = np.ones((mmpl)) * t
        plt.scatter(mmpl, mmp,c='r', s=40, linewidths="2", edgecolors='r')
    plt.ylabel('Azimuth [deg]', fontsize=28)
    plt.xlabel('Time index', fontsize=28)
    plt.xlim([0, len(maxmp)])
    plt.ylim([0, 360])
    plt.tight_layout()
    plt.savefig('../fig/max_dirs_'+name+'_a.png', dpi=200)

def musicpower_plot(power_list, name):
    import matplotlib.pyplot as plt
    import matplotlib
    from matplotlib.colors import BoundaryNorm
    from matplotlib.ticker import MaxNLocator
    matplotlib.rcParams['pdf.fonttype'] = 42
    matplotlib.rcParams['ps.fonttype'] = 42
    matplotlib.rc('xtick', labelsize=27)
    matplotlib.rc('ytick', labelsize=27)
    # music power plot
    x,y = power_list[0].shape
    x = range(x)
    y = np.linspace(500, 4000, y)
    x,y = np.meshgrid(x,y)

    for i,data in enumerate(power_list):
        plt.figure()
        #data /= data.max()
        levels = MaxNLocator(nbins=30).tick_values(0.001 + 0, data[:50, :50].max())
        cmap = plt.get_cmap('PiYG')

        plt.contourf(x, y, data.T, levels=levels,
                     cmap=cmap, extend='both')
        plt.xlabel('Azimuth [deg]', fontsize=28)
        plt.ylabel('Frequency [kHz]', fontsize=28)
        plt.yticks([1000, 2000, 3000, 4000], [str(1), str(2), str(3), str(4)])
        plt.xlim([0,40])
        plt.hot()
        plt.colorbar()
        plt.tight_layout()
        plt.savefig('../fig/mfig/'+name+'_music_power_' + str(i) + '.png', dpi=200)


if __name__=='__main__':
    fmusic = Freq_MUSIC()
