#!/usr/bin/env python
#coding:utf-8
import numpy as np
from LocalizeMUSIC import LocalizeMUSIC


class freq_weight_c():
    def __init__(self):
        self.sigth = 0.5
        self.sigmoid_alpha = 10.0

        self.msim_th= 0.90
        self.mp = 180
        self.th = 10
        self.cut_azi = 5

        self.music = LocalizeMUSIC()
        self.music.mic_position('../config/kurage_micpos.xml')
        self.music.Transfer_function()
        self.msim = np.zeros((self.music.sound_direction_num, self.music.freqency_bin_num))
        self.sim_music()
        self.music_filter_init()
        self.music_th_filter()

    def music_th_filter(self):
        self.thfilter = self.filter.sum(axis=0)


    def sigmoid(self, data):
        data =  1./(1.+np.exp(-self.sigmoid_alpha * (data-self.sigth)))
        return data
    def sim_music(self):
        for i in range(self.music.sound_direction_num):
            self.msim[i,:] =  np.abs((self.music.tf_music[self.mp]*self.music.tf_music[i].conj()).sum(axis=1)) \
            / (np.sqrt(np.abs(((self.music.tf_music[self.mp]*self.music.tf_music[self.mp].conj()).sum(axis=1)))) \
                    *np.sqrt(np.abs(((self.music.tf_music[i]*self.music.tf_music[i].conj()).sum(axis=1)))))


    def music_filter_init(self):
        self.msim[self.msim < self.msim_th] = self.msim_th
        self.msim -= self.msim_th
        self.msim /= self.msim.max()
        self.msim /= self.msim[-self.th+self.mp:self.th+1+self.mp].sum(axis=0)
        #self.msim[self.msim <= self.msim[self.mp,0]] = 0.0
        self.prob_cut()
        self.filter = self.msim[-self.th+self.mp:self.th+1+self.mp]
        self.dn, self.fn = self.filter.shape
        self.music_shift_array = np.zeros((self.music.sound_direction_num+self.th*2, self.fn))

    def prob_cut(self):
        _,fn = self.msim.shape
        cut_dir = np.arange(0,fn, dtype=np.float32)/ fn
        cut_dir = cut_dir * self.cut_azi
        cut_dir = np.floor(cut_dir).astype(np.int32)
        for i in range(fn):
            self.msim[:self.mp-cut_dir[i],i]=0
            self.msim[self.mp+cut_dir[i]+1:, i]= 0

    def music_filter(self, mdata, fw):
        mdata = mdata*fw

        self.music_shift_array[:self.th,:] = mdata[-self.th:,:]
        self.music_shift_array[self.th:-self.th,:] = mdata[:,:]
        self.music_shift_array[-self.th:,:] = mdata[:self.th,:]
        data = np.zeros_like(mdata)
        for i in range(self.dn):
            data += self.music_shift_array[i:i+self.music.sound_direction_num,:] * self.filter[i]

        return data




if __name__ == '__main__':
    import matplotlib.pyplot as plt
    import matplotlib
    from matplotlib.colors import BoundaryNorm
    from matplotlib.ticker import MaxNLocator
    matplotlib.rcParams['pdf.fonttype'] = 42
    matplotlib.rcParams['ps.fonttype'] = 42
    matplotlib.rc('xtick', labelsize=27)
    matplotlib.rc('ytick', labelsize=27)
    c = freq_weight_c()

    x = c.music.sound_direction
    y = c.music.frequency[c.music.low_freqency_idx:c.music.high_freqency_idx+1]
    x,y = np.meshgrid(x, y)

    levels = MaxNLocator(nbins=30).tick_values(0,c.msim.max())
    cmap = plt.get_cmap('PiYG')
    plt.contourf(x, y, c.msim.T, levels=levels,
                 cmap=cmap, extend='both')
    plt.xlabel('Azimuth [deg]', fontsize=28)
    plt.ylabel('Frequency [kHz]', fontsize=28)
    plt.hot()
    #plt.colorbar(ticks=[0,0.2,0.4,0.6,0.8,1])
    plt.colorbar()

    plt.yticks([1000,2000,3000,4000], [1,2,3,4])
    plt.xticks([0,150, 300])
    plt.xticks([150,180,210])

    plt.xlim([150, 210])
    plt.tight_layout()
    plt.savefig('./simi_3.png', dpi=200)
    plt.show()

