import numpy as np
from scipy.special import gammaln, psi

class pybnmf_poisson():
    def __init__(self):
        self.eps = np.spacing(1)
        self.ACTIVATION_WINDOW = 10
        self.frequency_num = 512 #fs:16000, window:1024
    def spectrogram_data(self, frequency_num):
        self.frequency_num = frequency_num


    def prior_param_set(self, a_tm, b_tm, a_ve, b_ve, I):
        #set prior parameters for gamma distribution
        #a:shape, b:scale w.r.t gamma dis, and tm:template, ve:Activation

        self.a_tm = a_tm * np.ones((self.frequency_num, I)) #Shape
        self.b_tm = b_tm * np.ones((self.frequency_num, I)) # Scale
        self.a_ve = a_ve * np.ones((I, self.ACTIVATION_WINDOW))
        self.b_ve = b_ve * np.ones((I, self.ACTIVATION_WINDOW))

        self.T = np.random.gamma(self.a_tm, self.b_tm)
        self.V = np.random.gamma(self.a_ve, self.b_ve)


        #return a_tm, b_tm, a_ve, b_ve

    def bayesian_nmf(self, X,  n_iter=50, Bound = 0):
        #initalize
        L_t = np.random.gamma(self.a_tm, self.b_tm/self.a_tm)
        L_v = np.random.gamma(self.a_ve, self.b_ve/self.a_ve)
        E_t = L_t.copy()
        E_v = L_v.copy()
        Sig_t = L_t.copy()
        Sig_v = L_v.copy()

        #For Bound caluration
        #cost = np.zeros(n_iter)
        if Bound == 1:
            gammalnX = gammaln(X+1)

        for i in range(n_iter):
            #Source sufficient stasistics
            LtLv = np.dot(L_t, L_v)
            tmp = X / LtLv

            Sig_t = L_t * np.dot(tmp, L_v.T)
            Sig_v = L_v * np.dot(L_t.T, tmp)

            #Means
            alpha_tm = self.a_tm + Sig_t
            beta_tm = 1/(self.a_tm/self.b_tm + E_v.sum(axis=1).T)
            E_t = alpha_tm * beta_tm

            alpha_ve = self.a_ve + Sig_v
            beta_ve = 1/((self.a_ve/self.b_ve).T + E_t.sum(axis=0)).T
            E_v = alpha_ve * beta_ve

            if Bound == 1:
                B_logT = np.log(L_t)
                B_logV = np.log(L_v)

                cost = -np.sum(np.dot(E_t, E_v) + gammalnX) \
                    + np.sum(-X * (((L_t * B_logT).dot(L_v) + L_t.dot(L_v * B_logV))/LtLv - np.log(LtLv))) \
                    + np.sum(-self.a_tm / self.b_tm * E_t - gammaln(self.a_tm) + self.a_tm*np.log(self.a_tm/self.b_tm)) \
                    + np.sum(-self.a_ve / self.b_ve * E_v - gammaln(self.a_ve) + self.a_ve*np.log(self.a_ve/self.b_ve)) \
                    + np.sum(gammaln(alpha_tm) + alpha_tm * (np.log(beta_tm) + 1)) \
                    + np.sum(gammaln(alpha_ve) + alpha_ve * (np.log(beta_ve) + 1))
                cost /= (float(self.ACTIVATION_WINDOW) * 10**6)





            if i == n_iter - 1:
                return E_t, E_v, cost

            #Means of Logs
            L_t = np.exp(psi(alpha_tm)) * beta_tm
            L_v = np.exp(psi(alpha_ve)) * beta_ve



if __name__ == '__main__':



    from pysound import pysound
    pys = pysound()
    pys.fileopen('../sound_data/mix1.wav')
    pys.fileopen('../sound_data/tnoise2.wav')
    #pys.fileopen('../sound_data/getuyoubi2.wav')

    pys.STFT_init()
    fft_frame = int(np.floor((pys.frames_num - pys.WINDOW)/float(pys.ADVANCE)))

    bnmf = pybnmf_poisson()
    bnmf.ACTIVATION_WINDOW = 10
    bnmf.spectrogram_data(pys.frequency_num)
    bnmf.prior_param_set(1,1,1,1,I=10)



    import time
    stime = time.time()
    fft_data_list = []
    nmf_data = np.zeros((pys.frequency_num, bnmf.ACTIVATION_WINDOW))

    nmf_wave = np.zeros(pys.WINDOW)
    temp_fft_list = []
    #NMF

    ST = []
    SV = []
    for i in range(fft_frame):
        start = i * pys.ADVANCE
        sound_data = pys.sound_data[:,start:start+pys.WINDOW]
        fft_data = pys.STFT(sound_data)
        temp_fft_list.append(fft_data)


        if (i+1)%bnmf.ACTIVATION_WINDOW == 0:
            fft_array = np.array(temp_fft_list)[:,0,:].T # (freq, frame)
            X = np.absolute(fft_array)
            angle = np.angle(fft_array)

            T,V,cost = bnmf.bayesian_nmf(X,  Bound=1)
            S = np.dot(T,V)
            print cost
            nmf_data = np.concatenate([nmf_data, S], axis=1)
            S = S*np.exp(1*angle)
            nmf_wdata = pys.ISTFT(S.T)
            for n in range(bnmf.ACTIVATION_WINDOW):
                nmf_wave = np.concatenate([nmf_wave, np.zeros(pys.ADVANCE)])
                ns = (i +1 - bnmf.ACTIVATION_WINDOW + n) * pys.ADVANCE

                nmf_wave[ns : ns + pys.WINDOW] += nmf_wdata[n]

            fft_data_list += temp_fft_list
            temp_fft_list = []


    nmf_wave = nmf_wave.reshape((1, nmf_wave.shape[0]))
    print nmf_wave

    print nmf_wave[0,10]
    output_f = './test.wav'
    pys.wave_write(nmf_wave, output_f)

    print 'Time:',time.time() - stime, ' [sec]'
    fft_data_array = np.array(fft_data_list)
    print '(time frame, channel, freq) = ',fft_data_array.shape

    import matplotlib.pyplot as plt
    x_shape = nmf_data.shape[1] - 50
    nmf_data = nmf_data[:,50:]
    x_data = np.arange(x_shape)
    X,Y = np.meshgrid(x_data,pys.frequency)
    nmf_data[nmf_data > 10000] = 10000
    plt.contourf(X,Y,nmf_data)

    plt.hot()
    plt.show()
