#!/usr/bin/env python
#coding:utf-8
import rospy
import numpy as np
import math
from scipy import signal
from LocalizeMUSIC import LocalizeMUSIC
from rospy.numpy_msg import numpy_msg
from rospy_tutorials.msg import Floats

import time
class Freq_MUSIC():
    def __init__(self):
        self.music = LocalizeMUSIC()
        self.music.frequency_init()
        #Estimation sound direction
        self.music_ith = 0.001
        self.music_th = 0.03 * self.music.freqency_bin_num
        self.music_dir_pi = self.music.sound_direction *np.pi / 180
        print 'music th:', self.music_th

        self.VonMises_direst = VonMises_Direction_Estimation()

        #id
        self.id_control = id_control()

        self.musicp_l = []
        self.music_ml = []
        self.music_power_list =[]

        # ROS
        rospy.init_node('Freq_MUSIC')
        hark_fft = rospy.Subscriber('/MUSIC_Power', numpy_msg(Floats), self.callback)
        self.sound_dir_pub = rospy.Publisher('/Temp_sound_dir', numpy_msg(Floats), queue_size=10)
        # FFT init
        self.mpower_diradd = 10
        self.mpower = np.zeros(self.music.sound_direction_num+self.mpower_diradd*2, dtype=np.float32)
        self.count = 0
        try:
            rospy.spin()
        except KeyboardInterrupt:
            pass
        self.music_power_list = np.array(self.music_power_list, dtype=np.float32)
        print self.music_power_list.shape
        np.save('music_power.npy', self.music_power_list)
        mplot(self.music.sound_direction, self.musicp_l, self.music_ml)
        #mplot(self.music.sound_direction, self.VonMises_direst.posdis_list, self.music_ml)

    def callback(self, data):
        print data.data.shape
        s = time.time()
        mpower = data.data.reshape((self.music.sound_direction_num, self.music.freqency_bin_num)).copy()
        self.music_power_list.append(mpower)
        uniform_dir, uniform_id = self.uniform_freq_music(mpower)

        self.VonMises_direst.RB_updata(uniform_id)
        Vondir, Vonid = self.return_maxdir(self.VonMises_direst.poster_dis, self.VonMises_direst.prob_th)
        Vondir = self.VonMises_direst.RB_maxID(Vondir)

        self.id_control.noise_dirs_input(Vondir)
        self.id_control.temp_dirs_input(uniform_dir)
        ids, dirs = self.id_control.return_True_direction()
        pub_data = np.append(ids, dirs).astype(np.float32)
        self.sound_dir_pub.publish(pub_data)

        #print
        print 'music observation:', uniform_dir
        print 'noise:', self.id_control.nid_array, self.id_control.ndir_array
        print 'temp:', self.id_control.tid_array, self.id_control.tdir_array
        print 'return:', dirs
        print time.time() - s,'secs'


    def uniform_freq_music(self, mpower):

        mth = np.where(mpower < self.music_ith)
        if mth[0].shape[0] > 0:
            mpower[mth[0], mth[1]] = self.music_ith/100.
        freq_inte_mpower = mpower.sum(axis=1)
        maxdir, maxId = self.return_maxdir(freq_inte_mpower, self.music_th)
        self.musicp_l.append(freq_inte_mpower)
        self.music_ml.append(maxdir)
        return maxdir, maxId

    def return_maxdir(self, dir_array, th):
        self.mpower[:self.mpower_diradd] = dir_array[-self.mpower_diradd:]
        self.mpower[-self.mpower_diradd:] = dir_array[:self.mpower_diradd]
        self.mpower[self.mpower_diradd:-self.mpower_diradd] = dir_array

        maxId = signal.argrelmax(self.mpower, order=3)[0]
        maxId = maxId[np.where((self.mpower_diradd-1 < maxId) &
                      (maxId < (self.music.sound_direction_num + self.mpower_diradd))
                        & (self.mpower[maxId] > th))[0]]
        maxId -= self.mpower_diradd
        max_dir = self.music.sound_direction[maxId]

        return max_dir,maxId

class VonMises_Direction_Estimation(LocalizeMUSIC):
    '''
    定常雑音を観測する確率分布
    '''
    def __init__(self):
        super(VonMises_Direction_Estimation, self).__init__()
        kap = 70
        theta_th = 3
        kernel_num = 11
        self.same_th = 5
        self.oktimes = 5
        self.pos_dadd = (kernel_num - 1)/2
        self.prob_th = 1./(self.sound_direction_num)
        self.noise_dirs = np.array([], dtype=np.float32)
        self.noise_dirs_count = np.array([], dtype=np.int32)
        self.x = np.array([], dtype=np.float32)
        self.y = np.array([], dtype=np.float32)

        dir_rad = self.sound_direction * np.pi / 180
        unit_vec = np.array([np.cos(dir_rad), np.sin(dir_rad)])
        self.likelihood = np.zeros((self.sound_direction_num, self.sound_direction_num), dtype=np.float32)

        for i, drad in enumerate(dir_rad):
            self.likelihood[i] = np.exp(kap * np.dot(unit_vec[:,i] , unit_vec[:,]))

        temp_th = self.likelihood[0, theta_th]
        self.likelihood[self.likelihood < temp_th] = temp_th
        self.likelihood /= self.likelihood.sum(axis=1)

        self.ones_dis = np.ones_like(self.likelihood[0])
        self.ones_dis /= self.ones_dis.sum()
        self.poster_dis = self.ones_dis.copy()
        #for recursive bayesian predict
        self.poster_dis_add = np.zeros(self.sound_direction_num + self.pos_dadd*2, dtype=np.float32)
        self.predict_kernel = np.ones(kernel_num)/float(kernel_num)

        self.posdis_list = []
        self.count = 0

    def RB_updata(self, thetas):
        self.noise_dirs_count += 1
        obdir = []
        for direction in thetas:
            x = np.array([math.cos(direction * math.pi / 180.)])
            y = np.array([math.sin(direction * math.pi / 180.)])
            theta = np.absolute(np.arccos((x*self.x + y*self.y)))
            wtheta = np.where(theta < self.same_th)[0]
            if wtheta.shape[0] > 0:
                self.noise_dirs_count[wtheta] = 0
            else:
                obdir.append(direction)

        thetas = np.array(obdir)
        #predict non shift
        self.poster_dis_add[:self.pos_dadd] = self.poster_dis[-self.pos_dadd:]
        self.poster_dis_add[-self.pos_dadd:] = self.poster_dis[:self.pos_dadd]
        self.poster_dis_add[self.pos_dadd:-self.pos_dadd] = self.poster_dis
        self.poster_dis = np.convolve(self.poster_dis_add, self.predict_kernel, 'valid')
        #updata
        num_thetas = thetas.shape[0]
        likelihood = self.ones_dis.copy()
        if num_thetas > 0:
            for i, dir in enumerate(thetas):
                likelihood[self.likelihood[int(dir)] > likelihood] = \
                    self.likelihood[int(dir)][self.likelihood[int(dir)] > likelihood]

            self.poster_dis *= likelihood
        th = self.prob_th / 10
        self.poster_dis[self.poster_dis > th] = th
        self.poster_dis /= self.poster_dis.sum()
        self.posdis_list.append(self.poster_dis)


        noise_del_count = np.where(self.noise_dirs_count > self.oktimes)[0]
        if noise_del_count.shape[0] > 0:
            for idx in noise_del_count:
                nid_ok_array = np.delete(self.noise_dirs, idx)
                nid_ok_count = np.delete(self.noise_dirs_count, idx)
                nx = np.delete(self.x, idx)
                ny = np.delete(self.y, idx)

            self.noise_dirs = nid_ok_array
            self.noise_dirs_count = nid_ok_count
            self.x = nx
            self.y = ny


    def RB_maxID(self, Vondir):
        for dir in Vondir:
            self.noise_dirs = np.append(self.noise_dirs, np.array([dir]))
            self.noise_dirs_count = np.append(self.noise_dirs_count, np.array([0]))
            self.x =  np.append(self.x, np.array(math.cos(dir * math.pi / 180.)))
            self.y =  np.append(self.y, np.array(math.sin(dir * math.pi / 180.)))
        return self.noise_dirs

class id_control():
    def __init__(self):
        self.nid_array = []
        self.ndir_array = []
        self.nx = np.array([], dtype=np.float32)
        self.ny = np.array([], dtype=np.float32)
        self.nid_ok_array = np.array([], dtype=np.bool)
        self.nid_ok_count = np.array([], dtype=np.int32)
        self.tid_array = []
        self.tdir_array = []
        self.tx = []
        self.ty = []
        self.tid_ok_count = []
        self.tid_ok_array = []

        self.temp2noise_idlist = []

        self.t_times = 3
        self.t_oktimes = 1
        self.t_same_th = 3. * math.pi/180
        self.cnoise_id_init = 100
        self.temp_id_init = 200

        self.same_th = 5. * math.pi /180.
        self.oktimes = 5
    def noise_dirs_input(self, dirs):
        self.nid_ok_array[:] = True
        self.nid_ok_count[:] += 1
        tx = np.array(self.tx)
        ty = np.array(self.ty)
        for i, direction in enumerate(dirs):
            x = np.array([math.cos(direction * math.pi / 180.)])
            y = np.array([math.sin(direction * math.pi / 180.)])
            theta = np.absolute(np.arccos((x*tx + y*ty)))
            wtheta = np.where(theta < self.same_th)[0]
            if wtheta.shape[0] > 0:
                #create noise direction from temp direction
                tid = wtheta[0]
                self.nid_array.append(self.cnoise_id_init)#self.tid_array[tid])
                self.ndir_array.append(self.tdir_array[tid])
                self.nx = np.append(self.nx, self.tx[tid])
                self.ny = np.append(self.ny, self.ty[tid])
                self.nid_ok_array = np.append(self.nid_ok_array, np.array([True], dtype=np.bool))
                self.nid_ok_count = np.append(self.nid_ok_count, np.array([1], dtype=np.int32))
                self.temp2noise_idlist.append(tid)
                self.cnoise_id_init += 1

            elif not len(self.nid_array):
                self.nid_array.append(self.cnoise_id_init)
                self.ndir_array.append(direction)

                self.nx = np.append(self.nx, x)
                self.ny = np.append(self.ny, y)
                self.nid_ok_array = np.append(self.nid_ok_array, np.array([True], dtype=np.bool))
                self.nid_ok_count = np.append(self.nid_ok_count, np.array([1], dtype=np.int32))
                self.cnoise_id_init += 1
            else:
                theta = np.absolute(np.arccos((x*self.nx + y*self.ny)))
                theta_num = np.where(theta < self.same_th)[0]

                if theta_num.shape[0]:
                    self.nid_ok_array[theta_num] = True
                    self.nid_ok_count[theta_num] = 1
                elif not theta_num.shape[0]:
                    #new noise direction
                    self.nid_array.append(self.cnoise_id_init)

                    self.ndir_array.append(direction)
                    self.nx = np.append(self.nx, x)
                    self.ny = np.append(self.ny, y)
                    self.nid_ok_array = np.append(self.nid_ok_array, np.array([True], dtype=np.bool))
                    self.nid_ok_count = np.append(self.nid_ok_count, np.array([1], dtype=np.int32))
                    self.cnoise_id_init += 1

        noise_del_count = np.where(self.nid_ok_count > self.oktimes)[0]
        if noise_del_count.shape[0] > 0:
            for idx in noise_del_count:
                print idx, noise_del_count, self.nid_ok_count, self.ndir_array, self.nid_ok_array
                self.ndir_array[idx] = ''
                self.nid_array[idx] = ''
                nid_ok_array = np.delete(self.nid_ok_array, idx)
                nid_ok_count = np.delete(self.nid_ok_count, idx)
                nx = np.delete(self.nx, idx)
                ny = np.delete(self.ny, idx)

            self.nid_array = filter(lambda s: s != '', self.nid_array)
            self.ndir_array = filter(lambda s: s != '', self.ndir_array)

            self.nid_ok_array = nid_ok_array
            self.nid_ok_count = nid_ok_count
            self.nx = nx
            self.ny = ny



        if len(self.temp2noise_idlist) > 0:
            for idk in self.temp2noise_idlist:
                self.tid_array[idk] = ''
                self.tdir_array[idk] = ''
                self.tx[idk] = ''
                self.ty[idk] = ''
                self.tid_ok_count[idk] = ''
                self.tid_ok_array[idk] = ''
            self.tid_array = filter(lambda s: s != '', self.tid_array)
            self.tdir_array = filter(lambda s: s != '', self.tdir_array)
            self.tx = filter(lambda s: s != '', self.tx)
            self.ty = filter(lambda s: s != '', self.ty)
            self.tid_ok_count = filter(lambda s: s != '', self.tid_ok_count)
            self.tid_ok_array = filter(lambda s: s != '', self.tid_ok_array)
            self.temp2noise_idlist = []

    def temp_dirs_input(self, dirs):
        self.tid_ok_count = map(lambda x:x+1,self.tid_ok_count)
        self.tid_ok_array = map(lambda x:False, self.tid_ok_array)
        for i, direction in enumerate(dirs):
            x = math.cos(direction * math.pi / 180.)
            y = math.sin(direction * math.pi / 180.)
            theta = np.absolute(np.arccos((x*self.nx + y*self.ny)))

            if not self.nid_ok_array[theta < self.same_th].shape[0]:
                if not len(self.tid_array):
                    self.tid_array.append(self.temp_id_init)
                    self.tdir_array.append(direction)
                    self.tx.append(x)
                    self.ty.append(y)
                    self.tid_ok_count.append(1)
                    self.tid_ok_array.append(True)
                    self.temp_id_init += 1
                else:
                    theta = np.absolute(np.arccos((x * np.array(self.tx) + y * np.array(self.ty))))
                    theta_thnum = np.where(theta < self.t_same_th)[0]
                    if theta_thnum.shape[0]:
                        for idk in theta_thnum:

                            self.tdir_array[idk] = direction
                            self.tx[idk] = x
                            self.ty[idk] = y
                            self.tid_ok_count[idk] = 1
                            self.tid_ok_array[idk] = True

                    elif not theta_thnum.shape[0]:
                        self.tid_array.append(self.temp_id_init)
                        self.tdir_array.append(direction)
                        self.tx.append(x)
                        self.ty.append(y)
                        self.tid_ok_count.append(1)
                        self.tid_ok_array.append(True)
                        self.temp_id_init += 1
        self.ok_add()
        self.del_count_max()

    def return_True_direction(self):


        ndir = np.array(self.ndir_array)[self.nid_ok_array]
        nid = np.array(self.nid_array)[self.nid_ok_array]
        print self.nid_ok_array

        tok = np.array(self.tid_ok_array, dtype=np.bool)
        tdir = np.array(self.tdir_array)[tok]
        tid = np.array(self.tid_array)[tok]

        ids = np.append(nid, tid)
        dirs = np.append(ndir, tdir)
        return ids, dirs
    def ok_add(self):
        for i, count in enumerate(self.tid_ok_count):
            if count <= self.t_oktimes:
                self.tid_ok_array[i] = True

    def del_count_max(self):
        max_ids = np.where(np.array(self.tid_ok_count) > self.t_times)[0]
        tfok = np.where(np.array(self.tid_ok_count) == False)[0]
        for idk in tfok:
            self.tid_ok_count[idk] += 1
        for idk in max_ids:
            self.tid_array[idk] = ''
            self.tdir_array[idk] = ''
            self.tx[idk] = ''
            self.ty[idk] = ''
            self.tid_ok_count[idk] = ''
            self.tid_ok_array[idk] = ''
        self.tid_array = filter(lambda  s:s != '', self.tid_array)
        self.tdir_array = filter(lambda  s:s != '', self.tdir_array)
        self.tx = filter(lambda  s:s != '', self.tx)
        self.ty = filter(lambda  s:s != '', self.ty)
        self.tid_ok_count = filter(lambda  s:s != '', self.tid_ok_count)
        self.tid_ok_array = filter(lambda  s:s != '', self.tid_ok_array)











def mplot(dir, mp, maxmp):
    import matplotlib.pyplot as plt
    import matplotlib
    from matplotlib.colors import BoundaryNorm
    from matplotlib.ticker import MaxNLocator
    matplotlib.rcParams['pdf.fonttype'] = 42
    matplotlib.rcParams['ps.fonttype'] = 42
    matplotlib.rc('xtick', labelsize=27)
    matplotlib.rc('ytick', labelsize=27)
    #music power plot
    data = np.array(mp)

    plt.figure()


    #data /= data.max()
    tn, dn = data.shape
    tn = range(tn)
    x, y = np.meshgrid(tn, dir)
    levels = MaxNLocator(nbins=30).tick_values(data.min() + 0, data.max())
    cmap = plt.get_cmap('PiYG')

    plt.contourf(x, y, data.T, levels=levels,
                 cmap=cmap, extend='both')
    plt.xlabel('Time index', fontsize=28)
    plt.ylabel('Frequency index', fontsize=28)
    plt.hot()
    plt.colorbar()
    plt.tight_layout()
    plt.savefig('../fig/music_power.png', dpi=200)

    #maxdir plot
    plt.figure()
    for t in tn:
        mmp = maxmp[t]
        mmpl = len(mmp)
        mmpl = np.ones((mmpl)) * t
        plt.scatter(mmpl, mmp,c='r', s=40, linewidths="2", edgecolors='r')
    plt.ylabel('Azimuth [deg]', fontsize=28)
    plt.xlabel('Time index', fontsize=28)
    plt.ylim([0, 60])
    plt.tight_layout()
    plt.savefig('../fig/max_dirs.png', dpi=200)
def musicpower_plot():
    pass

if __name__=='__main__':
    fmusic = Freq_MUSIC()
