#!/usr/bin/env python
#coding:utf-8
import rospy
import numpy as np
import math
from LocalizeMUSIC import LocalizeMUSIC
from rospy.numpy_msg import numpy_msg
from rospy_tutorials.msg import Floats
from hark_msgs.msg import HarkSource, HarkSourceVal

class Dir2HarkSrc():
    def __init__(self):
        self.music = LocalizeMUSIC()

        #ros
        rospy.init_node('Dir2HarkSrc')
        hark_dir = rospy.Subscriber('/Temp_sound_dir', numpy_msg(Floats), self.callback)
        self.sound_dir_pub = rospy.Publisher('/sound_dir', HarkSource, queue_size=10)
        self.HarkSrc = HarkSource()
        self.HarkSrc.count = 0
        try:
            rospy.spin()
        except KeyboardInterrupt:
            pass

    def callback(self, data):
        data = data.data.reshape((2,-1))
        _, src_num = data.shape
        self.HarkSrc.header.frame_id = 'HarkRosFrameID'

        self.HarkSrc.exist_src_num = src_num

        self.HarkSrc.src = []
        for i in range(src_num):
            self.HarkSrc.src.append(HarkSourceVal())
            self.HarkSrc.src[i].id = int(data[0,i])
            self.HarkSrc.src[i].azimuth = data[1,i]
            self.HarkSrc.src[i].x = \
                float(round(self.music.sound_distance * math.cos(math.radians(data[1,i])),4))
            self.HarkSrc.src[i].y = \
                float(round(self.music.sound_distance * math.sin(math.radians(data[1, i])),4))
            self.HarkSrc.src[i].power = 20.0
            self.HarkSrc.src[i].elevation = 0.0

        for _ in range(self.music.MUSIC_PERIOD):
            self.HarkSrc.header.stamp = rospy.Time.now()
            self.HarkSrc.count += 1
            self.HarkSrc.header.seq = self.HarkSrc.count
            for i in range(3):
                self.sound_dir_pub.publish(self.HarkSrc)

if __name__ == '__main__':
    d2hs = Dir2HarkSrc()