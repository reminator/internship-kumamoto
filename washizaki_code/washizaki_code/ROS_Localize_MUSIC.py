#!/usr/bin/env python
#coding:utf-8
import rospy
import numpy as np
from LocalizeMUSIC import LocalizeMUSIC
from hark_msgs.msg import HarkFFT
from rospy.numpy_msg import numpy_msg
from rospy_tutorials.msg import Floats

class ROS_Localize_MUSIC():
    def __init__(self):
        self.music = LocalizeMUSIC()

        # Transfer function
        self.music.mic_position('../config/kurage_micpos.xml')
        self.music.Transfer_function()

        # ROS
        rospy.init_node('Localize_MUSIC')
        hark_fft = rospy.Subscriber('/gHarkFFT', HarkFFT, self.callback)
        self.music_pub = rospy.Publisher('/MUSIC_Power', numpy_msg(Floats),queue_size=10)
        #FFT init
        self.fft_array = np.zeros((self.music.channel_num, self.music.frequency_num))
        self.count = 0
        try:
            rospy.spin()
        except KeyboardInterrupt:
            pass

    def callback(self, data):
        data = np.array([np.array(fdata.fftdata_real) + 1j * np.array(fdata.fftdata_imag) \
                for fdata in data.src])

        ok, music_result = self.music.localize_MUSIC(data)
        if ok:

            music_result = music_result.flatten()
            self.music_pub.publish(music_result)







if __name__ == '__main__':
    music_c = ROS_Localize_MUSIC()

