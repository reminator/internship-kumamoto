import matplotlib.pyplot as plt
import pyaudio
import numpy as np
from threading import Lock
import scipy.io.wavfile

mutex = Lock()

DEVICE_NAME = "TAMAGO-01"
RATE = 44100
NB_CHANNELS = 8
NB_VALUE = 0
NB_VALUE_MAX = 1000000
END_VALUE = 1000000
RECORDING_FORMAT = pyaudio.paInt16
DECODING_FORMAT = np.int16
end = False

# True : plot the signal while recording
plot = False

# True : Save in file
save = True
all_channels = True
CHANNEL = 6
NAME_FILE_SAVE = "./readAudio_results/ryoma_2_up"

DATA = {}

for i in range(NB_CHANNELS):
    DATA[i] = [0] * NB_VALUE_MAX


def get_input_device_index(name):
    for i in range(pa.get_device_count()):
        devinfo = pa.get_device_info_by_index(i)
        print("Device %d: %s" % (i, devinfo["name"]))
        if name in devinfo["name"]:
            return i
    return None


pa = pyaudio.PyAudio()

id_device = get_input_device_index(DEVICE_NAME)

assert id_device is not None
print("Name : %s\nId device : %d" % (DEVICE_NAME, id_device))


def callback(in_data, frame_count, _, __):
    mutex.acquire()
    global DATA
    global NB_VALUE
    global end

    data = np.frombuffer(in_data, dtype=DECODING_FORMAT)
    results = {}

    channels = int(len(data) / frame_count)
    for i in range(channels):
        results[i] = data[i::channels]

    for i in range(len(results[0])):
        for j in range(channels):
            if NB_VALUE == NB_VALUE_MAX:
                NB_VALUE = 0
            DATA[j][NB_VALUE] = results[j][i]
        NB_VALUE += 1
        if NB_VALUE == END_VALUE:
            end = True
            print("End recording")
            mutex.release()
            return in_data, pyaudio.paComplete

    mutex.release()
    return in_data, pyaudio.paContinue


stream = pa.open(
    rate=RATE,
    channels=NB_CHANNELS,
    format=RECORDING_FORMAT,
    # Start the recording imediatly
    input=True,
    input_device_index=id_device,
    stream_callback=callback,
)
print("Start recording")

while not end:
    if plot:
        mutex.acquire()
        print("Stop recording")
        plt.figure()
        for i in range(NB_CHANNELS):
            plt.subplot(int(NB_CHANNELS/2), 2, i+1)
            plt.plot(DATA[i])
            plt.title(str(i))
        print("Start recording")
        mutex.release()
        plt.pause(0.1)

mutex.acquire()
print("End program")
mutex.release()

stream.stop_stream()
stream.close()
pa.terminate()

plt.figure()
for i in range(NB_CHANNELS):
    plt.subplot(int(NB_CHANNELS/2), 2, i+1)
    plt.plot(DATA[i])
    plt.title(str(i))
plt.show()

if save:
    if not all_channels:
        scipy.io.wavfile.write(NAME_FILE_SAVE + ".wav", RATE, np.array(DATA[CHANNEL]).astype(np.int16))
    else:
        for i in range(NB_CHANNELS):
            scipy.io.wavfile.write(NAME_FILE_SAVE + "_chan_" + str(i) + ".wav", RATE, np.array(DATA[i]).astype(np.int16))
