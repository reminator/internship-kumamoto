clear;

%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Version history
%  - Version 2.0, October 2011: reduced the number of displayed digits
%  - Version 1.0, June 2010: first release
% Copyright 2010-2011 Valentin Emiya and Emmanuel Vincent (INRIA).
% This software is distributed under the terms of the GNU Public License
% version 3 (http://www.gnu.org/licenses/gpl.txt).
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%
% Set inputs
%%%%%%%%%%%%
ks = 1:50;

originalFiles = {
    '../../../../audio/audio/remi_7.wav';
    '../../../../audio/noise/husky_noise_1_+10dB.wav'    
    };
estimateFiles = "../../../../results/res_SSNMF_k" + ks + ".wav";


%%%%%%%%%%%%%
% Set options
%%%%%%%%%%%%%
options.destDir = 'res/';
options.segmentationFactor = 1; % increase this integer if you experienced "out of memory" problems

%%%%%%%%%%%%%%%%%%%%
% Call main function
%%%%%%%%%%%%%%%%%%%%
ress = [];

for k = ks
    tic;
    estimateFile = ['../../../../results/res_SSNMF_k' int2str(k) '.wav'];
    
    res = PEASS_ObjectiveMeasure(originalFiles,estimateFile,options);
    ress = [ress res]

    %%%%%%%%%%%%%%%%%
    % Display results
    %%%%%%%%%%%%%%%%%

    fprintf('************************\n');
    fprintf('* INTERMEDIATE RESULTS *\n');
    fprintf('************************\n');

    fprintf('The decomposition has been generated and stored in:\n');
    cellfun(@(s)fprintf(' - %s\n',s),res.decompositionFilenames);

    fprintf('The ISR, SIR, SAR and SDR criteria computed with the new decomposition are:\n');
    fprintf(' - SDR = %.1f dB\n - ISR = %.1f dB\n - SIR = %.1f dB\n - SAR = %.1f dB\n',...
        res.SDR,res.ISR,res.SIR,res.SAR);

    fprintf('The audio quality (PEMO-Q) criteria computed with the new decomposition are:\n');
    fprintf(' - qGlobal = %.3f\n - qTarget = %.3f\n - qInterf = %.3f\n - qArtif = %.3f\n',...
        res.qGlobal,res.qTarget,res.qInterf,res.qArtif);

    fprintf('*************************\n');
    fprintf('****  FINAL RESULTS  ****\n');
    fprintf('*************************\n');
    fprintf(' - Overall Perceptual Score: OPS = %.f/100\n',res.OPS)
    fprintf(' - Target-related Perceptual Score: TPS = %.f/100\n',res.TPS)
    fprintf(' - Interference-related Perceptual Score: IPS = %.f/100\n',res.IPS)
    fprintf(' - Artifact-related Perceptual Score: APS = %.f/100\n',res.APS);
    toc;
end
%%


SDRs = [];
ISRs = [];
SIRs = [];
SARs = [];

qGlobals = [];
qTargets = [];
qInterfs = [];
qArtifs = [];

OPSs = [];
TPSs = [];
IPSs = [];
APSs = [];


for res = ress
    SDRs = [SDRs res.SDR];
    ISRs = [ISRs res.ISR];
    SIRs = [SIRs res.SIR];
    SARs = [SARs res.SAR];

    qGlobals = [qGlobals res.qGlobal];
    qTargets = [qTargets res.qTarget];
    qInterfs = [qInterfs res.qInterf];
    qArtifs = [qArtifs res.qArtif];

    OPSs = [OPSs res.OPS];
    TPSs = [TPSs res.TPS];
    IPSs = [IPSs res.IPS];
    APSs = [APSs res.APS];
end
    
figure();
plot(ks, SDRs);
    
figure();
plot(ks, ISRs);
    
figure();
plot(ks, SIRs);
    
figure();
plot(ks, SARs);
    
figure();
plot(ks, qGlobals);
    
figure();
plot(ks, qTargets);
    
figure();
plot(ks, qInterfs);
    
figure();
plot(ks, qArtifs);
    
figure();
plot(ks, OPSs);
    
figure();
plot(ks, TPSs);
    
figure();
plot(ks, IPSs);
    
figure();
plot(ks, APSs);
%%
availableGPUs = gpuDeviceCount("available")
parpool(3);

spmd
  q = labindex;
end

q
delete(gcp);
