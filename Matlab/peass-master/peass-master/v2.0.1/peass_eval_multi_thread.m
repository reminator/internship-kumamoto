clear;

%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Version history
%  - Version 2.0, October 2011: reduced the number of displayed digits
%  - Version 1.0, June 2010: first release
% Copyright 2010-2011 Valentin Emiya and Emmanuel Vincent (INRIA).
% This software is distributed under the terms of the GNU Public License
% version 3 (http://www.gnu.org/licenses/gpl.txt).
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%
% Set inputs
%%%%%%%%%%%%
ks = 1:20:1000;
% mus = 0:20:1000;

originalFiles = {
    '../../../../audio/audio/Kihara_1_back_chan_0.wav';
    '../../../../audio/noise/husky_noise_1_speed2_chan_4.wav'    
    };
  

%%%%%%%%%%%%%
% Set options
%%%%%%%%%%%%%
options.destDir = 'res/';
options.segmentationFactor = 1; % increase this integer if you experienced "out of memory" problems

%%%%%%%%%%%%%%%%%%%%
% Call main function
%%%%%%%%%%%%%%%%%%%%

lenght = size(ks);
% lenght = size(mus);

nb_w = 4;
parpool(nb_w);

% k = 700;
mu = 1000;

spmd
    ress_pool = cell(1, lenght(2))
    
    for i = labindex:numlabs:lenght(2)
        tic;
        k = ks(i);
        % mu = mus(i);
        
        % estimateFile = ['../../../../results/res_PSNMF_n400_k' int2str(k) '_mu' int2str(mu) '.wav'];
        estimateFile = ['../../../../results/res_PSNMF_n1000_k' int2str(k) '_mu' int2str(mu) '.wav'];
        
        res = PEASS_ObjectiveMeasure(originalFiles,estimateFile,options);
        ress_pool{i} = res;
        toc;
    end
    
end


%ress_pool{1}
ress = cell(1, lenght(2));

for j = 1:nb_w
    for i = j:nb_w:lenght(2)
        resj = ress_pool{j};
        ress{i} = resj{i};
    end
end
    

%%

SDRs = [];
ISRs = [];
SIRs = [];
SARs = [];

qGlobals = [];
qTargets = [];
qInterfs = [];
qArtifs = [];

OPSs = [];
TPSs = [];
IPSs = [];
APSs = [];


for res_c = ress
    res = res_c{1};
    SDRs = [SDRs res.SDR];
    ISRs = [ISRs res.ISR];
    SIRs = [SIRs res.SIR];
    SARs = [SARs res.SAR];

    qGlobals = [qGlobals res.qGlobal];
    qTargets = [qTargets res.qTarget];
    qInterfs = [qInterfs res.qInterf];
    qArtifs = [qArtifs res.qArtif];

    OPSs = [OPSs res.OPS];
    TPSs = [TPSs res.TPS];
    IPSs = [IPSs res.IPS];
    APSs = [APSs res.APS];
end

xs = ks

%%
1:20:1001

%%

figure();
subplot(3,4,1);
plot(xs, SDRs, "-o");
title("SDR");
    
subplot(3,4,2);
plot(xs, ISRs, "-o");
title("ISR");
    
subplot(3,4,3);
plot(xs, SIRs, "-o");
title("SIR");
    
subplot(3,4,4);
plot(xs, SARs, "-o");
title("SAR");
    
subplot(3,4,5);
plot(xs, qGlobals, "-o");
title("qGlobal");
    
subplot(3,4,6);
plot(xs, qTargets, "-o");
title("qTarget");
    
subplot(3,4,7);
plot(xs, qInterfs, "-o");
title("qInterf");
    
subplot(3,4,8);
plot(xs, qArtifs, "-o");
title("qArtif");
    
subplot(3,4,9);
plot(xs, OPSs, "-o");
title("OPS");
    
subplot(3,4,10);
plot(xs, TPSs, "-o");
title("TPS");
    
subplot(3,4,11);
plot(xs, IPSs, "-o");
title("IPS");
    
subplot(3,4,12);
plot(xs, APSs, "-o");
title("APS");