PATH_AUDIO = "../../../audio/";
PATH_AUDIO_AUDIO = PATH_AUDIO + "audio/";
PATH_AUDIO_AUDIO_NOISE = PATH_AUDIO + "audio_noise/";
PATH_AUDIO_NOISE = PATH_AUDIO + "noise/";


file_an = PATH_AUDIO_AUDIO_NOISE + "auto_remi_7_husky_1_+10dB.wav";
file_a = PATH_AUDIO_AUDIO + "remi_7.wav";
file_n = PATH_AUDIO_NOISE + "husky_noise_1_+10dB.wav";
estimateFile = PATH_AUDIO + "../results/res_SSNMF_n400_k" + int2str(44) + ".wav";

[signal_an, fs_an] = audioread(file_an);
[signal_a, fs_a] = audioread(file_a);
[signal_n, fs_n] = audioread(file_n);

[se, fs_se] = audioread(estimateFile);
len_a = size(signal_a)

S = zeros(1, len_a(1));
N = zeros(1, len_a(1));

S(1, :) = signal_a(:);
N(1, :) = signal_n(:);

%
% Input:
%   - se: row vector of length T containing the estimated source,
%   - index: points which component of S se has to be compared to,
%   - S: n x T matrix containing the original sources,
%   - N: m x T matrix containing the noise on the obseravtions (if any).

[s_target, e_interf, e_noise, e_artif] = c2.1bss_decomp_gain(se, 1, S, N)