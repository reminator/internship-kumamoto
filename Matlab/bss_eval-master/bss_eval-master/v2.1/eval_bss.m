clear;
close all;

PATH_AUDIO = "../../../../audio/";
PATH_AUDIO_AUDIO = PATH_AUDIO + "audio/";
PATH_AUDIO_AUDIO_NOISE = PATH_AUDIO + "audio_noise/";
PATH_AUDIO_NOISE = PATH_AUDIO + "noise/";


file_an = PATH_AUDIO_AUDIO_NOISE + "auto_remi_7_husky_1_+10dB.wav";
file_a = PATH_AUDIO_AUDIO + "remi_7.wav";
file_n = PATH_AUDIO_NOISE + "husky_noise_1_+10dB.wav";

[signal_an, fs_an] = audioread(file_an);
[signal_a, fs_a] = audioread(file_a);
[signal_n, fs_n] = audioread(file_n);


len_a = size(signal_a)

se = zeros(1, len_a(1));
S = zeros(1, len_a(1));
N = zeros(1, len_a(1));

S(1, :) = signal_a(:);
N(1, :) = signal_n(:);

%
% Input:
%   - se: row vector of length T containing the estimated source,
%   - index: points which component of S se has to be compared to,
%   - S: n x T matrix containing the original sources,
%   - N: m x T matrix containing the noise on the obseravtions (if any).

ks = 1:1:672;
lenght = size(ks);

SDRs = zeros(1, lenght(2));
SIRs = zeros(1, lenght(2));
SNRs = zeros(1, lenght(2));
SARs = zeros(1, lenght(2));

i = 1;
for k = ks
    estimateFile = PATH_AUDIO + "../results/res_PSNMF_n400_k" + int2str(k) + ".wav";
    [signal_es, fs_se] = audioread(estimateFile);
    se(1, :) = signal_es(:);
    
    "Traitement de " + int2str(k)
    [s_target, e_interf, e_noise, e_artif] = bss_decomp_gain(se, 1, S, N);
    [SDRs(i), SIRs(i), SNRs(i), SARs(i)] = bss_crit(s_target, e_interf, e_noise, e_artif);
    i = i + 1;
end

xs = ks + ks(1);
    
figure();
plot(ks, SDRs);
title("SDR");
    
figure();
plot(ks, SIRs);
title("SIR");
    
figure();
plot(ks, SNRs);
title("SNR");
    
figure();
plot(ks, SARs);
title("SAR");
