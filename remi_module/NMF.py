import numpy as np

__authors__ = "Rémi Laborie"
__contact__ = "rlaborie2000@gmail.com"
__copyright__ = "Kumamoto University, Autonomous Systems Lab."
__date__ = "2022-07-28"


def NMF(X: np.ndarray, W: np.ndarray = None, H: np.ndarray = None, K: int = 2, max_iter: int = 100, err: float = 0.0,
        verbose: bool = False, norm: str = "fr"):
    """Compute NMF algorithm.

    This algorithm approximates a non-negative matrix X with 2 smaller non-negative matrices: X = W @ H.
    Cost function : ||X - W @ H||^2
    Sizes are defined below in brackets.

    :param X: Matrix to approximate (I, J). Spectrogram in audio purpose.
    :param W: Dictionary matrix (I, K). None for random start.
    :param H: Activation matrix (K, J). None for random start.
    :param K: Number of bases.
    :param max_iter: Number of iterations (recommended to set it to avoid infinity loop).
    :param err: The function will return the approximated X before max_iter reached if the norm of the difference with X is less than this number.
    :param verbose: Set True if you want to print explanation text.
    :param norm: The divergence to use: "fr" for Frobenius; "kl" for Kullback-Leibler; "is" for Itakura-Saito.

    :return: W, H
    """

    # Test the validity of parameters
    ch = check_and_set_NMF(X, W, H, K, norm)
    if type(ch) == str:
        print(ch)
        return

    W = ch["W"]
    H = ch["H"]
    beta = ch["beta"]

    # Multiplication update
    res = W @ H
    for _ in range(max_iter):

        num = ((X * res**(beta-2)) @ H.T)
        den = ((res**(beta-1)) @ H.T)
        W *= (num / den) ** phi(beta)
        res = W @ H

        num = (W.T @ (X * res**(beta-2)))
        den = (W.T @ (res**(beta-1)))
        H *= (num / den) ** phi(beta)
        res = W @ H

        if np.linalg.norm(X - res) < err:
            if verbose:
                print("Local minimum")
            return W, H
    if verbose:
        print("Max iter reached")
    return W, H


def check_and_set_NMF(X, W, H, K, norm):

    # norm
    betas = {
        "fr": 2,
        "kl": 1,
        "is": 0,
    }
    beta = None
    for key, value in betas.items():
        if norm.lower() == key.lower():
            beta = value
            break
    if beta is None:
        return "norm must be one of " + str([k for k in betas.keys()])

    # X
    if X is None or type(X) != np.ndarray:
        return "X must be a numpy.ndarray"

    if len(X.shape) != 2:
        return "X must have 2 dimensions"

    X_x = X.shape[0]
    X_y = X.shape[1]

    # W
    if W is None:
        W = np.random.rand(X_x, K)

    else:
        if type(W) != np.ndarray:
            return "W must be a numpy.ndarray"

        if len(W.shape) != 2:
            return "W must have 2 dimensions"

        if W.shape[0] != X_x:
            return "W dimensions are not correct (I)"
        K = W.shape[1]

    # H
    if H is None:
        H = np.random.rand(K, X_y)

    else:
        if type(H) != np.ndarray:
            return "H must be a numpy.ndarray"

        if len(H.shape) != 2:
            return "H must have 2 dimensions"

        if H.shape[1] != X_y:
            return "H dimensions are not correct (J)"

    # W @ H
    if W.shape[1] != H.shape[0]:
        return "W or H dimensions are not correct (K)"

    return {
        "W": W,
        "H": H,
        "beta": beta,
    }


def phi(beta):
    if beta < 1:
        return 1 / (2 - beta)
    if beta <= 2:
        return 1
    return 1 / (beta - 1)
