import numpy as np


def MNMF(Y, H=None, T=None, V=None, n_components=2, max_iter=1000, err=0.0001):

    if Y is None:
        return None


    n_mics = Y.shape[0]
    I = Y.shape[1]
    J = Y.shape[2]

    X = np.empty((I, J, n_mics, n_mics))
    print(X.shape)

    print("s")
    for i in range(I):
        for j in range(J):
            X[i, j] = np.sqrt(Y[:, i, j] * Y[:, i, j].conj().T)
            X[i, j] = X[i, j] / abs(X[i, j])
    print("e")

    if H is None:
        H = np.random.rand(I, n_components, n_mics, n_mics)
        H = H / np.trace(H)

    if T is None:
        T = np.random.rand(I, n_components)

    if V is None:
        V = np.random.rand(n_components, J)

    T_prev = T
    V_prev = V
    H_prev = H

    res = np.empty((I, J, n_mics, n_mics))

    for i in range(I):
        for j in range(J):
            res[i, j] = np.sum([H_prev[i, k] * T_prev[i, k] * V_prev[k, j] for k in range(n_components)])

    for _ in range(max_iter):

        for k in range(n_mics):
            for i in range(I):
                print("OUi")
                print(V_prev.shape, X[i, 0].shape, H_prev[i, k])
                num = sum([V_prev[k, j] * np.trace(X[i, j] @ H_prev[i, k]) for j in range(J)])
                den = sum([V_prev[k, j] * np.trace(res[i, j] @ H_prev[i, k]) for j in range(J)])
                T[i, k] = T_prev[i, k] * num / den

                print("TEST")
                print(res[i, :].shape)
                print(V_prev[k, :] * res[i, :].shape)
                print(np.sum(V_prev[k, :] * res[i, :]).shape)
                S1 = np.sum(V_prev[k, :] * res[i, :])
                H[i, k] = H_prev[i, k]

            for j in range(J):
                num = sum([T_prev[k, j] * np.trace(X[i, j] @ H[i, k]) for i in range(I)])
                den = sum([T_prev[k, j] * np.trace(res[i, j] @ H[i, k]) for i in range(I)])
                V[k, j] = T_prev[k, j] * num / den

        res = H * T @ V

        if np.linalg.norm(X - res) < err:
            print("Local minimum")
            return H, T, V

    print("Max iter reached")
    return H, T, V
