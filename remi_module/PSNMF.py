import numpy as np

__authors__ = "Rémi Laborie"
__contact__ = "rlaborie2000@gmail.com"
__copyright__ = "Kumamoto University, Autonomous Systems Lab."
__date__ = "2022-07-28"


def PSNMF(X: np.ndarray, W1: np.ndarray, H1: np.ndarray = None, W2: np.ndarray = None, H2: np.ndarray = None, K: int = 2, mu=0.1, max_iter: int = 100, err: float = 0.0, verbose: bool = False, fs: bool = False, norm: str = "fr"):
    """Compute PS-NMF algorithm.

    This algorithm approximates a non-negative matrix X with 4 smaller non-negative matrices: X = W1 @ H1 + W2 @ H2.
    Cost function : ||X - (W1 @ H1 + W2 @ H2)||^2 + mu * ||W1 @ W2||^2
    Sizes are defined below in brackets.

    :param X: Matrix to approximate (I, J). Spectrogram in audio purpose.
    :param W1: Dictionary matrix of the supervised part (I, N).
    :param H1: Activation matrix of the supervised part (N, J). None for random start.
    :param W2: Dictionary matrix (I, K). None for random start.
    :param H2: Activation matrix (K, J). None for random start.
    :param K: Number of bases.
    :param mu: Coef of the cost function. With mu = 0, you have SS-NMF.
    :param max_iter: Number of iterations (recommended to set it to avoid infinity loop).
    :param err: The function will return the approximated X before max_iter reached if the norm of the difference with X is less than this number.
    :param verbose: Set True if you want to print explanation text.
    :param fs: Set True if the value of W1 and W2 are known to do full supervised NMF. False if you want W2 as an init value.
    :param norm: The divergence to use: "fr" for Frobenius; "kl" for Kullback-Leibler; "is" for Itakura-Saito.

    :return: W1, H1, W2, H2
    """

    # Test the validity of parameters
    ch = check_and_set_PSNMF(X, W1, H1, W2, H2, K, norm)
    if type(ch) == str:
        if verbose:
            print(ch)
        return

    H1 = ch["H1"]
    W2 = ch["W2"]
    H2 = ch["H2"]
    beta = ch["beta"]

    # Multiplication update
    res1 = W1 @ H1
    res2 = W2 @ H2
    res = res1 + res2
    for _ in range(max_iter):

        num = W1.T @ (X * np.power(res, beta-2))
        den = W1.T @ np.power(res, beta-1)
        H1 *= np.power(num / den, phi(beta))
        res1 = W1 @ H1
        res = res1 + res2

        if not fs:
            num = ((X * np.power(res, beta-2)) @ H2.T)
            den = (np.power(res, beta-1) @ H2.T + 2.0 * mu * (W1 @ (W1.T @ W2)))
            W2 *= np.power(num / den, phi(beta))
            res2 = W2 @ H2
            res = res1 + res2

        num = (W2.T @ (X * np.power(res, beta-2)))
        den = (W2.T @ np.power(res, beta-1))
        H2 *= np.power(num / den, phi(beta))
        res2 = W2 @ H2
        res = res1 + res2

        if np.linalg.norm(X - res) < err:
            print("Local minimum")
            return W1, H1, W2, H2

    print("Max iter reached")
    return W1, H1, W2, H2


def check_and_set_PSNMF(X, W1, H1, W2, H2, K, norm):

    # norm
    betas = {
        "fr": 2,
        "kl": 1,
        "is": 0,
    }
    beta = None
    for key, value in betas.items():
        if norm.lower() == key.lower():
            beta = value
            break
    if beta is None:
        return "norm must be one of " + str([k for k in betas.keys()])

    # X
    if X is None or type(X) != np.ndarray:
        return "X must be a numpy.ndarray"

    if len(X.shape) != 2:
        return "X must have 2 dimensions"

    X_x = X.shape[0]
    X_y = X.shape[1]

    # W1
    if W1 is None or type(W1) != np.ndarray:
        return "W1 must be a numpy.ndarray"

    if len(W1.shape) != 2:
        return "W1 must have 2 dimensions"

    if W1.shape[0] != X_x:
        return "W1 dimensions are not correct (I)"

    N = W1.shape[1]

    # H1
    if H1 is None:
        H1 = np.random.rand(N, X_y)

    else:
        if type(H1) != np.ndarray:
            return "H1 must be a numpy.ndarray"

        if len(H1.shape) != 2:
            return "H1 must have 2 dimensions"

        if H1.shape[1] != X_y:
            return "H1 dimensions are not correct (J)"

    # W1 @ H1
    if W1.shape[1] != H1.shape[0]:
        return "W1 or H1 dimensions are not correct (N)"

    # W2
    if W2 is None:
        W2 = np.random.rand(X_x, K)

    else:
        if type(W2) != np.ndarray:
            return "W2 must be a numpy.ndarray"

        if len(W2.shape) != 2:
            return "W2 must have 2 dimensions"

        if W2.shape[0] != X_x:
            return "W2 dimensions are not correct (I)"
        K = W2.shape[1]

    # H2
    if H2 is None:
        H2 = np.random.rand(K, X_y)

    elif type(H2) != np.ndarray:
        return "H2 must be a numpy.ndarray"

    elif len(H2.shape) != 2:
        return "H2 must have 2 dimensions"

    elif H2.shape[1] != X_y:
        return "H2 dimensions are not correct (J)"

    # W2 @ H2
    if W2.shape[1] != H2.shape[0]:
        return "W2 and H2 dimensions are not correct (K)"

    return {
        "H1": H1,
        "W2": W2,
        "H2": H2,
        "beta": beta,
    }


def phi(beta):
    if beta < 1:
        return 1 / (2 - beta)
    if beta <= 2:
        return 1
    return 1 / (beta - 1)
