import numpy as np

__authors__ = "Rémi Laborie"
__contact__ = "rlaborie2000@gmail.com"
__copyright__ = "Kumamoto University, Autonomous Systems Lab."
__date__ = "2022-07-28"


def KL_SSNMF(Y, W1, H1=None, W2=None, H2=None, mu=0, n_components=2, max_iter=1000, err=0.0001):
    """Deprecated version, you should use PSNMF with norm="kl"

    """

    if Y is None:
        return None

    if W1 is None:
        return None

    W1_y = W1.shape[1]
    signal_x = Y.shape[0]
    signal_y = Y.shape[1]

    if H1 is None:
        H1 = np.random.rand(W1_y, signal_y)

    if W2 is None:
        W2 = np.random.rand(signal_x, n_components)
        fs = False
    else:
        fs = True

    if H2 is None:
        H2 = np.random.rand(n_components, signal_y)

    ress = []
    res = W1 @ H1 + W2 @ H2
    if fs:
        for _ in range(max_iter):

            H2 *= (W2.T @ (Y / res)) / (np.ones((W2.shape[0], H2.shape[1])) @ W2.T)
            res = W1 @ H1 + W2 @ H2

            H1 *= (W1.T @ (Y / res)) / (W1.T @ np.ones((W1.shape[0], H1.shape[1])))
            res = W1 @ H1 + W2 @ H2

            ress.append(np.linalg.norm(Y - res))
            if np.linalg.norm(Y - res) < err:
                print("Local minimum")
                return W1, H1, W2, H2
    else:
        for _ in range(max_iter):

            H1 *= (W1.T @ (Y / res)) / (W1.T @ np.ones((W1.shape[0], H1.shape[1])))
            res = W1 @ H1 + W2 @ H2

            W2 *= ((Y / res) @ H2.T) / (np.ones((W2.shape[0], H2.shape[1])) @ H2.T + 2.0 * mu * (W1 @ (W1.T @ W2)))
            res = W1 @ H1 + W2 @ H2

            H2 *= (W2.T @ (Y / res)) / (W2.T @ np.ones((W2.shape[0], H2.shape[1])))
            res = W1 @ H1 + W2 @ H2

            ress.append(np.linalg.norm(Y - res))
            if np.linalg.norm(Y - res) < err:
                print("Local minimum")
                return W1, H1, W2, H2

    print("Max iter reached")
    return W1, H1, W2, H2, ress
